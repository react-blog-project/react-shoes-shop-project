import React, { MouseEventHandler } from "react"
import "./Button.scss"

const getLabelCls = (className: string, disabled: boolean = false) => {
  let clsArray = [""]

  if (className) {
    clsArray.push(className)
  }
  if (disabled) {
    clsArray.push("disabled")
  }
  return clsArray.join(" ")
}

type ButtonProps = {
  name?: string
  type: "button" | "reset" | "submit"
  value: any
  className?: string
  disabled?: boolean
  onClick?: MouseEventHandler<HTMLButtonElement>
}

export default function Button({
  name = "",
  type,
  value,
  className = "",
  disabled = false,
  onClick
}: ButtonProps) {
  const labelCls = getLabelCls(className, disabled)
  return (
    <button
      className={labelCls}
      name={name}
      type={type}
      disabled={disabled}
      onClick={onClick}
    >
      {value}
    </button>
  )
}
