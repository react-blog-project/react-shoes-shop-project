import { ReactComponent as Explore } from "../../../assets/images/explore-nav_mobile.svg"
import "./InputNav.scss"

export default function InputNav() {
  return (
    <div>
      <Explore className="explore-input" />
      <input type="text" placeholder="Search Product" className="input-field" />
    </div>
  )
}
