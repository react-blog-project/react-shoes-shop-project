import "./IconNav.scss"
import { ReactComponent as Heart } from "../../../assets/images/heart.svg"
import { ReactComponent as Noti } from "../../../assets/images/noti.svg"

export default function IconNav() {
  return (
    <div>
      <Noti />
      <Heart />
    </div>
  )
}
