import { Link } from "react-router-dom"
import { ReactComponent as Cart } from "../../../assets/images/cart.svg"
import { ReactComponent as Explore } from "../../../assets/images/explore-nav_mobile.svg"
import { ReactComponent as Home } from "../../../assets/images/home-nav_mobile.svg"
import { ReactComponent as Offer } from "../../../assets/images/offer.svg"
import { ReactComponent as Profile } from "../../../assets/images/profile.svg"
import "./BottomNav.scss"

export default function BottomNav() {
  return (
    <div>
      <div className="navbar navbar-bottom">
        <Link to="">
          <Home className="home" />
        </Link>
        <Link to="/explore">
          <Explore className="explore" />
        </Link>
        <Link to="/cart">
          <Cart className="cart" />
        </Link>
        <Link to="/offer">
          <Offer className="offer" />
        </Link>
        <Link to="/account">
          <Profile className="account" />
        </Link>
      </div>
    </div>
  )
}
