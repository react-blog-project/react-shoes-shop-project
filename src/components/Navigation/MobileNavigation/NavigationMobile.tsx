import IconNav from "./IconNav"
import InputNav from "./InputNav"

export default function NavigationMobile() {
  return (
    <div>
      <div className="d-flex">
        <div className="col-8 col-sm-9">
          <InputNav />
        </div>
        <div className="col-4 col-sm-3">
          <IconNav />
        </div>
      </div>
    </div>
  )
}
