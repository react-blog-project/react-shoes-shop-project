import { BREAKPOINT } from "./../../constants/styles"
import Menu from "./Menu"
import BottomNav from "./MobileNavigation/BottomNav"
import NavigationMobile from "./MobileNavigation/NavigationMobile"
import { useWindowSize } from "../../hooks/useWindowSize"

export default function Navigation() {
  const [width] = useWindowSize()
  const renderDesktopNav = width <= BREAKPOINT.MD ? null : <Menu />
  const renderMobileNav =
    width <= BREAKPOINT.MD ? (
      <div>
        <NavigationMobile />
        <BottomNav />
      </div>
    ) : null

  return (
    <div>
      {renderMobileNav}
      {renderDesktopNav}
    </div>
  )
}
