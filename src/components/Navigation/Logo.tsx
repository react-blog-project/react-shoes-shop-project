import { ReactComponent as LogoMain } from "../../assets/images/logo.svg"
import "./Logo.scss"

export default function Logo() {
  return (
    <span>
      <LogoMain className="logo-home" />
      <span className="logo-name">E-comm</span>
    </span>
  )
}
