import Logo from "./Logo"
import "./Menu.scss"

export default function Menu() {
  return (
    <div>
      <nav className="navbar p-0">
        <div className="container-fluid">
          <Logo />
          <form className="d-flex form">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-inline mx-auto ul">
              <li className="nav-item d-inline">
                <a
                  className="nav-link active d-inline list-menu"
                  aria-current="page"
                  href="/#"
                >
                  Home
                </a>
              </li>

              <div className="dropdown-navbar text-center">
                <div className="row ">
                  <div className="col-4">
                    <a href="/#">Catergory</a>
                  </div>
                  <div className="col-4">
                    <a href="/#">Catergory</a>
                  </div>
                  <div className="col-4">
                    <a href="/#">Catergory</a>
                  </div>
                </div>
              </div>

              <li className="nav-item d-inline ">
                <a
                  className="nav-link active d-inline list-menu"
                  aria-current="page"
                  href="/#"
                >
                  Bag
                </a>
              </li>
              <li className="nav-item d-inline">
                <a
                  className="nav-link active d-inline list-menu"
                  aria-current="page"
                  href="/#"
                >
                  Sneaker
                </a>
              </li>
              <li className="nav-item d-inline ">
                <a
                  className="nav-link active d-inline list-menu"
                  aria-current="page"
                  href="/#"
                >
                  Belt
                </a>
              </li>
              <li className="nav-item d-inline ">
                <a
                  className="nav-link active d-inline list-menu"
                  aria-current="page"
                  href="/#"
                >
                  Contact
                </a>
              </li>
            </ul>
          </form>
        </div>
      </nav>
    </div>
  )
}
