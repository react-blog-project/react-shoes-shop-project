import { ReactComponent as IconDelete } from "../../assets/images/Account/icon_delete.svg"
import Button from "../../components/Button/Button"
import PopConfirm from "../PopConfirm/PopConfirm"
import "./AddressItem.scss"

interface Props {
  name: string
  mobile: string
  address: Address
  onCancel(value?: string): void
  onDelete(value?: string): void
}

function AddressItem(props: Props) {
  const { name, mobile, address, onCancel, onDelete } = props

  const handleAddress = `${address.street}, ${address.ward}, ${address.district}, ${address.province}`
  return (
    <div className="address-item p-4 my-3">
      <p className="address-item__name">{name}</p>
      <p className="address-item__address">{handleAddress}</p>
      <p className="address-item__phone">{mobile}</p>
      <div className="address-item__button">
        <Button
          type="button"
          value="Edit"
          className="address-item__button--left"
        />
        <PopConfirm
          title="Do you want to delete this item ?"
          okText="Delete"
          onCancel={onCancel}
          onConfirm={onDelete}
        >
          <IconDelete className="address-item__button--right" />
        </PopConfirm>
      </div>
    </div>
  )
}

export default AddressItem
