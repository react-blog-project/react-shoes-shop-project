import React from "react"
import { connect } from "react-redux"
import { ReactComponent as DelButton } from "../../../assets/images/del.svg"
import ButtonGroup from "../../ButtonGroup/ButtonGroup"
import * as actions from "../../../pages/Cart/Cart.actions"
import "./TableRowData.scss"

const mapDispatchToProps = dispatch => ({
  removeSelectedItem: (index: number) =>
    dispatch(actions.removeSelectedItem(index))
})

const connector = connect(null, mapDispatchToProps)

type Props = {
  price: number
  removeSelectedItem(index: number): void
  id: number
  quantity: number
  index: number
  productId: number
  name: string
}

function TableRowData(props: Props) {
  const { price, removeSelectedItem, id, quantity, index, productId, name } =
    props

  return (
    <>
      <tr>
        <th scope="row">
          <DelButton
            className="btn-del"
            onClick={() => removeSelectedItem(index)}
          />
          <img
            src="https://s3-alpha-sig.figma.com/img/35ad/c5db/6d8623ccd841d6b09e2d57514c88b74b?Expires=1631491200&Signature=Wq6PS9e4thr1lWbwhygqS7nxkapmoMpjc4-B~Q~uMtgWRHcDP-1pEwow4fw8D79Povk6orsFvtXk6yyViuS3GjdykMHdWduV0HZaaTbZjyBN5tyu~9guYdGLZyTaQnjtVqK8DnW9RG31GCmxg9wYJtZpKsUtt3xVORxpehf0ug0U7MIZPnQZ0ErzJa2AxcUy8HFkyLst-1txIGmAxr6zSrTjTPq~Xs181sTjcVpZv5eq3A8CASvYlXJbh-wF1ysMX1UGNcyi-dd13KTKVNf125ntMFnRUm87tvN8bUQt5je7USE9qtXd-NADaimjCQL83~~AnXkZSuc0IJ1v1Jfg8g__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
            alt="img"
            className="img-cart"
          />
          <p>{name}</p>
        </th>
        <td className="td-price">{`$${price * quantity}`}</td>
        <td>
          <ButtonGroup
            id={id}
            quantity={quantity}
            productId={productId}
            totalPrice={price * quantity}
          />
        </td>
        <td className="td-price">{`$${price}`}</td>
      </tr>
    </>
  )
}

export default connector(TableRowData)
