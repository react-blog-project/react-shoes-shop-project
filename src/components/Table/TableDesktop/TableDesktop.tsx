import React from "react"
import { connect, ConnectedProps } from "react-redux"
import TableRowData from "./TableRowData"
import "./TableDesktop.scss"

const mapStateToProps = (state: AppState) => ({
  cartList: state.cart.cartList
})

const connector = connect(mapStateToProps)

interface Props extends ConnectedProps<typeof connector> {}

function TableDesktop(props: Props) {
  const { cartList } = props

  return (
    <div className="table-desktop">
      {cartList && cartList.length !== 0 ? (
        <>
          <table className="table table-product">
            <thead>
              <tr>
                <th scope="col">PRODUCT</th>
                <th scope="col">PRICE</th>
                <th scope="col">QTY</th>
                <th scope="col">UNIT PRICE</th>
              </tr>
            </thead>
            <tbody>
              {cartList.map((item, index) => {
                return (
                  <TableRowData
                    key={index}
                    price={item.product.salePrice}
                    id={item.id}
                    quantity={item.productNumber}
                    index={index}
                    productId={item.product.id}
                    name={item.product.name}
                  />
                )
              })}
            </tbody>
          </table>
        </>
      ) : (
        ""
      )}
    </div>
  )
}

export default connector(TableDesktop)
