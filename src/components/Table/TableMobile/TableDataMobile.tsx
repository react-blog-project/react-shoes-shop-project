import React from "react"
import { connect } from "react-redux"
import { ReactComponent as HeartIcon } from "../../../assets/images/heart.svg"
import { ReactComponent as GarbageIcon } from "../../../assets/images/garbage.svg"
import ButtonGroup from "../../ButtonGroup/ButtonGroup"
import * as actions from "../../../pages/Cart/Cart.actions"

const mapDispatchToProps = dispatch => ({
  removeSelectedItem: (index: number) =>
    dispatch(actions.removeSelectedItem(index))
})

const connector = connect(null, mapDispatchToProps)

type Props = {
  price: number
  removeSelectedItem: any
  id: number
  quantity: number
  index: number
  name: string
  productId: number
}

function TableDataMobile(props: Props) {
  const { price, removeSelectedItem, id, quantity, index, name, productId } =
    props

  return (
    <div className="row">
      <div className="col-3">
        <img
          src="https://s3-alpha-sig.figma.com/img/35ad/c5db/6d8623ccd841d6b09e2d57514c88b74b?Expires=1631491200&Signature=Wq6PS9e4thr1lWbwhygqS7nxkapmoMpjc4-B~Q~uMtgWRHcDP-1pEwow4fw8D79Povk6orsFvtXk6yyViuS3GjdykMHdWduV0HZaaTbZjyBN5tyu~9guYdGLZyTaQnjtVqK8DnW9RG31GCmxg9wYJtZpKsUtt3xVORxpehf0ug0U7MIZPnQZ0ErzJa2AxcUy8HFkyLst-1txIGmAxr6zSrTjTPq~Xs181sTjcVpZv5eq3A8CASvYlXJbh-wF1ysMX1UGNcyi-dd13KTKVNf125ntMFnRUm87tvN8bUQt5je7USE9qtXd-NADaimjCQL83~~AnXkZSuc0IJ1v1Jfg8g__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
          alt="img"
          className="list-img-product"
        />
      </div>
      <div className="col-9 d-flex align-content-between flex-wrap">
        <div className="col-12">
          <span className="product-name">{name}</span>
          <span className="icon">
            <HeartIcon className="heart-mobile" />
            <GarbageIcon onClick={() => removeSelectedItem(index)} />
          </span>
        </div>
        <div className="col-12">
          <span className="fontsize-mobile">{`$${price * quantity}`}</span>
          <span className="btn-group-mobile">
            <ButtonGroup
              id={id}
              quantity={quantity}
              productId={productId}
              totalPrice={price * quantity}
            />
          </span>
        </div>
      </div>
    </div>
  )
}
export default connector(TableDataMobile)
