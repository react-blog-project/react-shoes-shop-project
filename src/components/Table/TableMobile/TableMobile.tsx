import React from "react"
import { connect, ConnectedProps } from "react-redux"
import TableDataMobile from "./TableDataMobile"
import "./TableMobile.scss"

const mapStateToProps = (state: AppState) => ({
  cartList: state.cart.cartList
})

const connector = connect(mapStateToProps)

interface Props extends ConnectedProps<typeof connector> {}

function TableMobile(props: Props) {
  const { cartList } = props

  return (
    <div className="table-mobile">
      {cartList.map((item, index) => {
        return (
          <TableDataMobile
            key={index}
            price={item.product.salePrice}
            id={item.id}
            quantity={item.productNumber}
            index={index}
            productId={item.product.id}
            name={item.product.name}
          />
        )
      })}
    </div>
  )
}
export default connector(TableMobile)
