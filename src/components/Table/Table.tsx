import { BREAKPOINT } from "../../constants/styles"
import TableDesktop from "./TableDesktop/TableDesktop"
import TableMobile from "./TableMobile/TableMobile"
import { useWindowSize } from "../../hooks/useWindowSize"

export default function Table() {
  const [width] = useWindowSize()
  const renderTableDesktop = width >= BREAKPOINT.LG ? <TableDesktop /> : null
  const renderTableMobile = width < BREAKPOINT.LG ? <TableMobile /> : null
  return (
    <div>
      {renderTableDesktop}
      {renderTableMobile}
    </div>
  )
}
