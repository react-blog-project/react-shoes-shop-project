import React, { MouseEventHandler, ReactNode } from "react"
import "./Authentication.scss"

type AuthenticationProps = {
  children: ReactNode
  title: string
  onClick?: MouseEventHandler<HTMLDivElement>
}

export default function Authentication({
  children,
  title,
  onClick
}: AuthenticationProps) {
  return (
    <div onClick={onClick} className="authentication">
      <div className="d-flex py-3 text-center">
        {children}
        <span className="px-3 title flex-fill">{title}</span>
      </div>
    </div>
  )
}
