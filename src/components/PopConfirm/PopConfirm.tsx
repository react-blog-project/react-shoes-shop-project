import { ReactNode, useState } from "react"
import Button from "../../components/Button/Button"
import "./PopConfirm.scss"

type PopConfirmProps = {
  children: ReactNode
  title: string
  cancelText?: string
  okText?: string
  onCancel(value?: string): void
  onConfirm(value?: string): void
}

export default function PopConfirm(Props: PopConfirmProps) {
  const {
    children,
    title = "",
    cancelText = "Cancel",
    okText = "Ok",
    onCancel,
    onConfirm
  } = Props
  const [isToggle, setIsToggle] = useState(false)
  const handleCancel = () => {
    setIsToggle(false)
    onCancel()
  }
  const handleConfirm = () => {
    setIsToggle(false)
    onConfirm()
  }
  const handleClick = () => {
    setIsToggle(true)
  }

  return (
    <div className="d-inline">
      <span onClick={handleClick}> {children}</span>
      <div className="container-confirm">
        {isToggle && (
          <div className="pop-confirm d-block ">
            <p>
              <span>!</span> {title}
            </p>
            <div className="pop-confirm--button">
              <Button
                type="button"
                value={cancelText}
                onClick={handleCancel}
                className="pop-confirm--cancel"
              />
              <Button
                type="button"
                value={okText}
                onClick={handleConfirm}
                className="pop-confirm--ok"
              />
            </div>
          </div>
        )}
      </div>
    </div>
  )
}
