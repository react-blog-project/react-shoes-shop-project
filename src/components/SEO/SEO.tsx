import React from "react"
import { Helmet } from "react-helmet-async"
interface Props {
  title: string
  description: string
}

export default function SEO(props: Props) {
  const { title, description } = props

  return (
    <Helmet>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta
        id="keywords"
        name="keywords"
        content="Shopping, aibles, shoes shop"
      />
      <meta name="copyright" content="Aibes web dev" />
      <meta name="author" content="Aibles web dev" />
    </Helmet>
  )
}
