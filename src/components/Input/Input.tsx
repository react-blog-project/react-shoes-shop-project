import React, { FormEventHandler, ReactNode } from "react"
import "./Input.scss"

/**
 * getLabelCls
 * get class for label text
 */
const getLabelCls = (className: string, disabled: boolean = false) => {
  let clsArray = ["input"]

  if (className) {
    clsArray.push(className)
  }
  if (disabled) {
    clsArray.push("disabled")
  }
  return clsArray.join(" ")
}

type InputProps = {
  label?: string
  children?: ReactNode
  name: string
  registerInput: any
  type: string
  placeholder?: string
  errorMessage?: string
  errors?: any
  className?: string
  disabled?: boolean
  onChange: FormEventHandler<HTMLInputElement>
}

export default function Input({
  label = "",
  children,
  name,
  registerInput,
  type,
  placeholder,
  errorMessage,
  errors = "",
  className = "",
  disabled = false,
  onChange
}: InputProps) {
  const labelCls = getLabelCls(className, disabled)
  const handleChange = (value: React.ChangeEvent<HTMLInputElement>) => {
    onChange(value)
  }

  return (
    <label className={labelCls}>
      {label && <span>{label}</span>}
      <div className="content__input">
        {children}
        <input
          type={type}
          placeholder={placeholder}
          className={errorMessage || errors ? "err" : ""}
          disabled={disabled}
          {...registerInput(name)}
          onChange={handleChange}
        />
      </div>
      {errors && <p>{errors.message}</p>}
    </label>
  )
}
