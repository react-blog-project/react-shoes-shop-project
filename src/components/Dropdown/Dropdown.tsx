import React, { useState } from "react"
import { ReactComponent as IconDown } from "../../assets/images/icon_down.svg"
import "./Dropdown.scss"

const getLabelCls = (className: string, disabled: boolean = false) => {
  let clsArray = ["dropdown"]

  if (className) {
    clsArray.push(className)
  }
  if (disabled) {
    clsArray.push("disabled")
  }
  return clsArray.join(" ")
}

type DropdownProps = {
  value: string
  label?: string
  option: string[]
  placeholder?: string
  errorMessage?: string
  errors?: string
  className?: string
  disabled?: boolean
  onClick?: Function
  onChange(value: string): void
}

export default function Dropdown({
  value,
  label,
  option,
  errorMessage,
  errors = "",
  className = "",
  disabled = false,
  onClick = Function,
  onChange
}: DropdownProps) {
  const labelCls = getLabelCls(className, disabled)
  const [isOpen, setIsOpen] = useState(false)

  const selectItem = (event: string) => {
    setIsOpen(!isOpen)
    onChange(event)
  }
  const handleClick = () => {
    setIsOpen(!isOpen)
    onClick()
  }

  return (
    <label className={labelCls}>
      {label && <span className="dropdown__title">{label}</span>}
      <div className="dropdown__content">
        <div
          className={`dropdown__select ${errorMessage || errors ? "err" : ""}`}
          onClick={() => handleClick()}
        >
          <span className="dropdown__select--value">{value}</span>
          <IconDown className="dropdown__select--icon" />
        </div>
        {errors && <p className="error m-0">{errors}</p>}

        {isOpen && (
          <div className="dropdown__list">
            {option.map((item, key) => (
              <div
                key={key}
                onClick={() => selectItem(item)}
                className="dropdown__list--item"
              >
                {item}
              </div>
            ))}
          </div>
        )}
      </div>
    </label>
  )
}
