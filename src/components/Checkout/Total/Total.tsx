import React, { useState, useEffect } from "react"
import { connect, ConnectedProps } from "react-redux"
import Button from "src/components/Button/Button"
import "./Total.scss"

const mapStateToProps = (state: AppState) => ({
  cartList: state.cart.cartList
})

const connector = connect(mapStateToProps)

interface Props extends ConnectedProps<typeof connector> {}

function Total(props: Props) {
  const { cartList } = props
  const shipFee = 300
  const coupon = 200
  const [total, setTotal] = useState(0)

  useEffect(() => {
    let subTotal = 0
    cartList.forEach(item => {
      subTotal += item.product.salePrice * item.productNumber
    })
    setTotal(subTotal)
  }, [cartList])

  return (
    <div>
      <table className="table table-borderless">
        <tbody className="fontsize-mobile">
          <tr>
            <th scope="row">Subtotal</th>
            <td>{`$${total}`}</td>
          </tr>
          <tr>
            <th scope="row">Shipping fee</th>
            <td>{`$${shipFee}`}</td>
          </tr>
          <tr>
            <th scope="row">Coupon</th>
            <td>{`$${coupon}`}</td>
          </tr>
          <tr className="tr-total">
            <th scope="col">Total</th>
            <th className="float-right" scope="col">
              {`$${total + shipFee - coupon}`}
            </th>
          </tr>
        </tbody>
      </table>
      <Button className="btn-checkout" value="Check out" type="button" />
    </div>
  )
}
export default connector(Total)
