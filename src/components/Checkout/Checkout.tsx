import React from "react"
import { connect, ConnectedProps } from "react-redux"
import Total from "./Total/Total"
import Voucher from "./Voucher/Voucher"
import "./Checkout.scss"

const mapStateToProps = (state: AppState) => ({
  cartList: state.cart.cartList
})

const connector = connect(mapStateToProps)

interface Props extends ConnectedProps<typeof connector> {}

function Checkout(props: Props) {
  const { cartList } = props

  return (
    <>
      {cartList && cartList.length !== 0 ? (
        <div className="row checkout">
          <div className="col-12 col-lg-6">
            <Voucher />
          </div>
          <div className="col-12 col-lg-6">
            <Total />
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  )
}

export default connector(Checkout)
