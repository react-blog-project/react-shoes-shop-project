import React from "react"
import Button from "src/components/Button/Button"
import "./Voucher.scss"

export default function Voucher() {
  return (
    <div className="d-flex form-voucher">
      <input
        type="text"
        className="form-control voucher-input"
        placeholder="Voucher code"
      />
      <Button type="button" value="Apply" className="btn-voucher" />
    </div>
  )
}
