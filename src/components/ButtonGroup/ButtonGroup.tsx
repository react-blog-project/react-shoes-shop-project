import React, { useState } from "react"
import { connect } from "react-redux"
import Button from "../Button/Button"
import * as actions from "../../pages/Cart/Cart.actions"
import { updateCartList } from "src/pages/Cart/Cart.thunk"
import "./ButtonGroup.scss"

const mapStateToProps = (state: AppState) => ({})

const mapDispatchToProps = dispatch => ({
  onAddItem: (id: number, productNumber: number) =>
    dispatch(actions.onAddItem(id, productNumber)),
  onRemoveItem: (id: number, productNumber: number) =>
    dispatch(actions.onRemoveItem(id, productNumber)),
  updateCartList: (params: IupdateCart) => dispatch(updateCartList(params))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

type Props = {
  quantity?: number
  onAddItem(id: number, quantity: number): void
  onRemoveItem(id: number, quantity: number): void
  id?: number
  updateCartList: any
  productId?: number
  totalPrice?: number
}

function ButtonGroup(props: Props) {
  const [counter, setCounter] = useState(1)
  const {
    quantity = counter,
    onAddItem,
    onRemoveItem,
    id,
    updateCartList,
    productId,
    totalPrice
  } = props

  const decrement = () => {
    if (id) {
      onRemoveItem(id, quantity)
      updateCartList({
        quantity: quantity - 1,
        totalPrice: totalPrice,
        productId: productId
      })
    }
    if (counter > 1) {
      setCounter(counter - 1)
    }
  }
  const increment = () => {
    if (id) {
      onAddItem(id, quantity)
      updateCartList({
        quantity: quantity + 1,
        totalPrice: totalPrice,
        productId: productId
      })
    } else {
      setCounter(counter + 1)
    }
  }

  return (
    <div className="btn-groups">
      <Button
        className="btn-decre"
        value="-"
        type="button"
        onClick={decrement}
      />
      {quantity ? (
        <Button className="sum-value" value={quantity} type="button" />
      ) : (
        <Button className="sum-value" value={counter} type="button" />
      )}
      <Button
        className="btn-incre"
        value="+"
        type="button"
        onClick={increment}
      />
    </div>
  )
}

export default connector(ButtonGroup)
