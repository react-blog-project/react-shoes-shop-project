import { ReactComponent as WesternUnion } from "../../assets/images/western-union.svg"
import { ReactComponent as MasterCard } from "../../assets/images/master-card.svg"
import { ReactComponent as Paypal } from "../../assets/images/paypal.svg"
import { ReactComponent as Visa } from "../../assets/images/visa.svg"

import "./Brands.scss"

export default function Brands() {
  return (
    <div className="brands">
      <WesternUnion className="item" />
      <MasterCard className="item" />
      <Paypal className="item" />
      <Visa className="item" />
    </div>
  )
}
