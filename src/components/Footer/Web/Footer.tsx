import { ReactComponent as LogoIcon } from "../../../assets/images/logo.svg"
import { ReactComponent as Facebook } from "../../../assets/images/facebook.svg"
import { ReactComponent as Twitter } from "../../../assets/images/twitter.svg"
import Brands from "../Brands"
import "./Footer.scss"

export default function Footer() {
  return (
    <footer className="container-fluid footer">
      <div className="row row-1 align-items-center justify-content-between">
        <div className="col col-3">
          <div className="logo__box">
            <LogoIcon className="logo__icon" />
            <span className="shop__name">E-Comm</span>
          </div>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever.Since the 1500s, when an unknown printer.
          </p>
        </div>
        <div className="col col-3">
          <h4>Follow Us</h4>
          <p>
            Since the 1500s, when an unknown printer took a galley of type and
            scrambled.
          </p>
          <div>
            <Facebook className="social" />
            <Twitter className="social" />
          </div>
        </div>
        <div className="col col-2">
          <h4>Contact Us</h4>
          <p>E-Comm , 4578 Marmora Road, Glasgow D04 89GR</p>
        </div>
      </div>
      <div className="row row-2 align-items-center justify-content-between">
        <div className="col col-2">
          <h4>Information</h4>
          <a href="/#">About Us</a>
          <a href="/#">Information</a>
          <a href="/#">Privacy Policy</a>
          <a href="/#">Terms & Conditions</a>
        </div>
        <div className="col col-2">
          <h4>Service</h4>
          <a href="/#">About Us</a>
          <a href="/#">Information</a>
          <a href="/#">Privacy Policy</a>
          <a href="/#">Terms & Conditions</a>
        </div>
        <div className="col col-2">
          <h4>My Account</h4>
          <a href="/#">About Us</a>
          <a href="/#">Information</a>
          <a href="/#">Privacy Policy</a>
          <a href="/#">Terms & Conditions</a>
        </div>
        <div className="col col-2">
          <h4>Our Offers</h4>
          <a href="/#">About Us</a>
          <a href="/#">Information</a>
          <a href="/#">Privacy Policy</a>
          <a href="/#">Terms & Conditions</a>
        </div>
      </div>
      <div className="line"></div>
      <Brands />
    </footer>
  )
}
