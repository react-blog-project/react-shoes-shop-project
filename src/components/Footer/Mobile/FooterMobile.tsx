import { useState } from "react"
import DropdownButton from "../../../components/Footer/Mobile/DropdownButton"
import Brands from "../Brands"
import { ReactComponent as LogoIcon } from "../../../assets/images/logo.svg"
import { ReactComponent as Facebook } from "../../../assets/images/facebook.svg"
import { ReactComponent as Twitter } from "../../../assets/images/twitter.svg"
import "./FooterMobile.scss"
export default function FooterMobile() {
  const [isOpen, setIsOpen] = useState([false, false, false, false])
  return (
    <div className="footer__mobile">
      <div className="head">
        <div className="logo__box">
          <LogoIcon className="logo__icon" />
          <span className="shop__name"> E-Comm</span>
        </div>
        <div>
          <Facebook className="social" />
          <Twitter className="social" />
        </div>
      </div>
      <DropdownButton
        title="Information"
        onClick={() => setIsOpen([!isOpen[0], false, false, false])}
      >
        {isOpen[0] && (
          <div className="dropdown">
            <a href="/#">About Us</a>
            <a href="/#">Informations</a>
            <a href="/#">Primacy Policy</a>
            <a href="/#">Term & Conditions</a>
          </div>
        )}
      </DropdownButton>
      <DropdownButton
        title="Service"
        onClick={() => setIsOpen([false, !isOpen[1], false, false])}
      >
        {isOpen[1] && (
          <div className="dropdown">
            <a href="/#">About Us</a>
            <a href="/#">Informations</a>
            <a href="/#">Primacy Policy</a>
            <a href="/#">Term & Conditions</a>
          </div>
        )}
      </DropdownButton>
      <DropdownButton
        title="My Account"
        onClick={() => setIsOpen([false, false, !isOpen[2], false])}
      >
        {isOpen[2] && (
          <div className="dropdown">
            <a href="/#">About Us</a>
            <a href="/#">Informations</a>
            <a href="/#">Primacy Policy</a>
            <a href="/#">Term & Conditions</a>
          </div>
        )}
      </DropdownButton>
      <DropdownButton
        title="Our Offers"
        onClick={() => setIsOpen([false, false, false, !isOpen[3]])}
      >
        {isOpen[3] && (
          <div className="dropdown">
            <a href="/#">About Us</a>
            <a href="/#">Informations</a>
            <a href="/#">Primacy Policy</a>
            <a href="/#">Term & Conditions</a>
          </div>
        )}
      </DropdownButton>
      <h6>Contact Us</h6>
      <p>E-Comm , 4578 Marmora Road, Glasgow D04 89GR</p>
      <Brands />
    </div>
  )
}
