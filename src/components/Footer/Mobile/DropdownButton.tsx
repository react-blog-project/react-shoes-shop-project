import { MouseEventHandler, ReactNode } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleDown } from "@fortawesome/free-solid-svg-icons"
import "./DropdownButton.scss"

type DropdownButtonProps = {
  title: string
  children: ReactNode
  onClick?: MouseEventHandler<HTMLDivElement>
}
export default function DropdownButton({
  title,
  children,
  onClick
}: DropdownButtonProps) {
  return (
    <div className="menu" onClick={onClick}>
      <div className="title">
        <span>{title}</span>
        <FontAwesomeIcon className="iconMenu" icon={faAngleDown} />
      </div>
      {children}
    </div>
  )
}
