import React, { ReactNode } from "react"
import "./TitleHeader.scss"

type InputProps = {
  content: string
  children?: ReactNode
}

export default function TitleHeader(props: InputProps) {
  const { content, children } = props
  return (
    <div className="header-title align-items-center">
      {children}
      <span className="header-title__content">{content}</span>
    </div>
  )
}
