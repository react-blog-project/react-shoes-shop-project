import "./PageLoader.scss"
import SPIN from "../../assets/images/Spin.gif"

export default function PageLoader() {
  return (
    <div className="loading">
      <div className="loading__content">
        <img src={SPIN} alt="loading icon" />
      </div>
    </div>
  )
}
