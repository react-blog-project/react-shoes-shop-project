import "./LoadingLogin.scss"
import { ReactComponent as LoadingIcon } from "../../assets/images/singleIconWhite.svg"

export default function LoadingLogin() {
  return (
    <div className="container">
      <div className="content">
        <LoadingIcon />
        <div className="content__img"></div>
      </div>
    </div>
  )
}
