import { MouseEventHandler } from "react"
import { ReactComponent as RatingIcon } from "../../assets/images/rating.svg"
import "./ProductItem.scss"

type ProductProps = {
  product: Product
  onClick?: MouseEventHandler<HTMLDivElement>
}

export default function Product({ product, onClick }: ProductProps) {
  const items = [1, 2, 3, 4, 5]

  return (
    <div onClick={onClick} className="product">
      <div className="product__content">
        <img src={product.images[0].url} alt="product" />
        <div className="information">
          <p className="information__name mt-2 my-0">{product.name}</p>
          <div className="information__rating">
            {items.map((item, index) => (
              <RatingIcon
                className={item <= product.rate ? "rating-yellow" : ""}
                key={index}
              />
            ))}
          </div>
          <p className="information__currentPrice mb-2">${product.salePrice}</p>
          <div className="information__sale d-flex">
            <span className="priceMd">${product.salePrice}</span>
            <span className="information__sale--left pr-2">
              ${product.sellingPrice}
            </span>
            <span className="information__sale--right">24% Off</span>
          </div>
        </div>
      </div>
    </div>
  )
}
