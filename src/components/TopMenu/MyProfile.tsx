import { Link } from "react-router-dom"
import { ReactComponent as Cart } from "../../assets/images/cart.svg"
import { ReactComponent as Profile } from "../../assets/images/profile_close.svg"
import { ReactComponent as Search } from "../../assets/images/search.svg"
import { CURRENCY_DEFAULT, PATH } from "../../constants"
import "./MyProfile.scss"

const renderTotalItem = (total: number) => {
  if (total <= 0) return null
  const totalItemFormat = total > 9 ? "9+" : total
  return (
    <span className="total position-absolute badge rounded-pill">
      {totalItemFormat}
    </span>
  )
}

function MyProfile() {
  return (
    <div className="profile d-flex align-items-center">
      <Link
        className="link-profile d-flex align-items-center"
        to={PATH.ACCOUNT}
      >
        <Profile className="my-profile" />
        My account
      </Link>
      <Link to={PATH.CART} className="cart">
        <div className="position-relative">
          <Cart className="cart-top-menu" />
          {renderTotalItem(10)}
        </div>
        <span className="items">Items</span>
        <span className="amount">{`${CURRENCY_DEFAULT}${"0.00"}`}</span>
      </Link>
      <Search />
    </div>
  )
}

export default MyProfile
