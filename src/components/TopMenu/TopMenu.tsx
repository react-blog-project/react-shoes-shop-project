import MyProfile from "./MyProfile"
import MenuLeft from "./MenuLeft"
import "./TopMenu.scss"

export default function TopMenu() {
  return (
    <div>
      <header className="d-flex justify-content-between header">
        <MenuLeft />
        <MyProfile />
      </header>
    </div>
  )
}
