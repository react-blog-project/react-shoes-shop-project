import { ReactComponent as ArrowDown } from "../../assets/images/arrow_down.svg"
import "./MenuLeft.scss"

function IconMenuLeft(props) {
  return (
    <div className="menu-left__content">
      <span>{props.content}</span>
      {props.children}
    </div>
  )
}

export default function MenuLeft() {
  return (
    <div className="menu-left d-flex">
      <IconMenuLeft content="EN">
        <ArrowDown />
      </IconMenuLeft>
      <IconMenuLeft content="USD">
        <ArrowDown />
      </IconMenuLeft>
    </div>
  )
}
