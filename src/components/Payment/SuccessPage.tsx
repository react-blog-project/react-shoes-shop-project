import { useHistory } from "react-router-dom"
import Button from "../Button/Button"
import { ReactComponent as Back } from "../../assets/images/arrow_left.svg"
import { ReactComponent as Close } from "../../assets/images/times.svg"
import { PATH } from "../../constants"
import { BREAKPOINT } from "../../constants/styles"
import "./SuccessPage.scss"

function SuccessPage() {
  // Back to cart page
  const history = useHistory()
  const handleGoBack = () => {
    history.push(PATH.CART)
  }
  // Change text in button depend window's width
  const width = window.innerWidth
  let text = ""
  if (width <= BREAKPOINT.MD) text = "Back To Order"
  else text = "Complete"

  return (
    <div className="success__page">
      <div className="control hidden">
        <Back />
        <Close />
      </div>
      <div className="title hidden">Make Payment</div>
      <div className="process hidden">
        <hr />
        <div className="process__box">1</div>
        <div className="process__box">2</div>
        <div className="process__box">3</div>
      </div>
      <div className="success__box">
        <div className="icon__check"></div>
      </div>
      <h4>Success</h4>
      <span>thank you for shopping using lafyuu</span>
      <Button
        name="back-button"
        type="button"
        value={text}
        onClick={handleGoBack}
      />
    </div>
  )
}
export default SuccessPage
