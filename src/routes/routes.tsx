import { BrowserRouter } from "react-router-dom"
import LoginRoutes from "./LoginRoutes"
import RegisterRoutes from "./RegisterRoutes"
import HomeRoutes from "./HomeRoutes"
import ForgotPasswordRoutes from "./ForgotPasswordRoutes"
import AccountRoutes from "./AccountRoutes"
import ProfileRoutes from "./Account/ProfileRoutes"
import AddressRoutes from "./Account/AddressRoutes"
import AddAddressRoutes from "./Account/AddAddressRoutes"
import ChangePasswordRoutes from "./ChangePasswordRoutes"
import VerifyForgotPasswordRouters from "./VerifyForgotPasswordRouters"
import ChangeProfileRoutes from "./Account/ChangeProfileRoutes"
import CartRoutes from "./CartRoutes"
import ProductDetailsRoutes from "./Product/ProductDetails"

export default function Routes() {
  return (
    <BrowserRouter>
      <HomeRoutes />
      <LoginRoutes />
      <RegisterRoutes />
      <ForgotPasswordRoutes />
      <AccountRoutes />
      <ProfileRoutes />
      <ChangeProfileRoutes />
      <AddressRoutes />
      <AddAddressRoutes />
      <ChangePasswordRoutes />
      <VerifyForgotPasswordRouters />
      <CartRoutes />
      <ProductDetailsRoutes />
    </BrowserRouter>
  )
}
