import { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../constants"
import LoadingLogin from "../components/Loading/LoadingLogin"
import PublicGuard from "../guards/PublicGuard"
const ForgotPassword = lazy(
  () => import("../pages/ForgotPassword/ForgotPassword")
)

export default function LoginRoutes() {
  return (
    <Switch>
      <PublicGuard
        path={PATH.FORGOT_PASSWORD}
        component={() => (
          <Suspense fallback={<LoadingLogin />}>
            <ForgotPassword />
          </Suspense>
        )}
      />
    </Switch>
  )
}
