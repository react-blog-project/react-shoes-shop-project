import { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../constants"
import LoadingLogin from "../components/Loading/LoadingLogin"
import PublicGuard from "../guards/PublicGuard"
const Register = lazy(() => import("../pages/Register/Register"))

export default function RegisterRoutes() {
  return (
    <Switch>
      <PublicGuard
        path={PATH.REGISTER}
        component={() => (
          <Suspense fallback={<LoadingLogin />}>
            <Register />
          </Suspense>
        )}
      />
    </Switch>
  )
}
