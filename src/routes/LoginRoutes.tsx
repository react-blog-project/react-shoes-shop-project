import { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../constants"
import LoadingLogin from "../components/Loading/LoadingLogin"
import PublicGuard from "../guards/PublicGuard"
const Login = lazy(() => import("../pages/Login/Login"))

export default function LoginRoutes() {
  return (
    <Switch>
      <PublicGuard
        exact
        path={PATH.LOGIN}
        component={() => (
          <Suspense fallback={<LoadingLogin />}>
            <Login />
          </Suspense>
        )}
      />
    </Switch>
  )
}
