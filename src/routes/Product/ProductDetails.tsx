import { lazy, Suspense } from "react"
import { Route, Switch } from "react-router-dom"
import { PATH } from "../../constants"
import Loading from "../../components/Loading/Loading"
const ProductDetails = lazy(
  () => import("../../pages/Product/ProductDetails/ProductDetails")
)

export default function ProductDetailsRoutes() {
  return (
    <Switch>
      <Route
        path={`${PATH.PRODUCT}/:slug`}
        component={() => (
          <Suspense fallback={<Loading />}>
            <ProductDetails />
          </Suspense>
        )}
      />
    </Switch>
  )
}
