import React, { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../../constants"
import AuthenticatedGuard from "../../guards/AuthenticatedGuard"
import Loading from "../../components/Loading/Loading"
const AddAddress = lazy(
  () => import("../../pages/Address/AddAddress/AddAddress")
)

export default function AddAddressRoutes() {
  return (
    <Switch>
      <AuthenticatedGuard
        path={PATH.ADD_ADDRESS}
        component={() => (
          <Suspense fallback={<Loading />}>
            <AddAddress />
          </Suspense>
        )}
      />
    </Switch>
  )
}
