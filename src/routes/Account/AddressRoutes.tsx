import React, { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../../constants"
import AuthenticatedGuard from "../../guards/AuthenticatedGuard"
import Loading from "../../components/Loading/Loading"
const AddressList = lazy(
  () => import("../../pages/Address/AddressList/AddressList")
)

export default function AddressRoutes() {
  return (
    <Switch>
      <AuthenticatedGuard
        path={PATH.ADDRESS}
        component={() => (
          <Suspense fallback={<Loading />}>
            <AddressList />
          </Suspense>
        )}
      />
    </Switch>
  )
}
