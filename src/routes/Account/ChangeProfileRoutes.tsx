import React, { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../../constants"
import AuthenticatedGuard from "../../guards/AuthenticatedGuard"
import Loading from "../../components/Loading/Loading"
const ChangeProfile = lazy(
  () => import("../../pages/ChangeProfile/ChangeProfile")
)

export default function ChangeProfileRoutes() {
  return (
    <Switch>
      <AuthenticatedGuard
        path={PATH.CHANGE_PROFILE}
        component={() => (
          <Suspense fallback={<Loading />}>
            <ChangeProfile />
          </Suspense>
        )}
      />
    </Switch>
  )
}
