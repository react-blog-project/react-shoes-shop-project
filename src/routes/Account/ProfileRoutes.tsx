import React, { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../../constants"
import AuthenticatedGuard from "../../guards/AuthenticatedGuard"
import Loading from "../../components/Loading/Loading"
const Profile = lazy(() => import("../../pages/Profile/Profile"))

export default function ProfileRoutes() {
  return (
    <Switch>
      <AuthenticatedGuard
        path={PATH.PROFILE}
        component={() => (
          <Suspense fallback={<Loading />}>
            <Profile />
          </Suspense>
        )}
      />
    </Switch>
  )
}
