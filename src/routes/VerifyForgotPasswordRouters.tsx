import { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../constants"
import LoadingLogin from "../components/Loading/LoadingLogin"
import PublicGuard from "../guards/PublicGuard"
const VerifyForgotPassword = lazy(
  () => import("../pages/VerifyForgotPassword/VerifyForgotPassword")
)

export default function VerifyForgotPasswordRoutes() {
  return (
    <Switch>
      <PublicGuard
        path={PATH.VERIFY_FORGOT_PASSWORD}
        component={() => (
          <Suspense fallback={<LoadingLogin />}>
            <VerifyForgotPassword />
          </Suspense>
        )}
      />
    </Switch>
  )
}
