import { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../constants"
import LoadingLogin from "../components/Loading/Loading"
import AuthenticatedGuard from "../guards/AuthenticatedGuard"
const ChangePassword = lazy(
  () => import("../pages/ChangePassword/ChangePassword")
)

export default function LoginRoutes() {
  return (
    <Switch>
      <AuthenticatedGuard
        path={PATH.CHANGE_PASSWORD}
        component={() => (
          <Suspense fallback={<LoadingLogin />}>
            <ChangePassword />
          </Suspense>
        )}
      />
    </Switch>
  )
}
