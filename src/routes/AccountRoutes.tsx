import { lazy, Suspense } from "react"
import { Route, Switch } from "react-router-dom"
import { PATH } from "../constants"
import Loading from "../components/Loading/Loading"
const Account = lazy(() => import("../pages/Account/Account"))

export default function AccountRoutes() {
  return (
    <Switch>
      <Route
        exact
        path={PATH.ACCOUNT}
        component={() => (
          <Suspense fallback={<Loading />}>
            <Account />
          </Suspense>
        )}
      />
    </Switch>
  )
}
