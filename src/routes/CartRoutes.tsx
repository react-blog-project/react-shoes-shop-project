import { lazy, Suspense } from "react"
import { Switch } from "react-router-dom"
import { PATH } from "../constants"
import Loading from "../components/Loading/Loading"
import AuthenticatedGuard from "../guards/AuthenticatedGuard"
const Cart = lazy(() => import("../pages/Cart/Cart"))

export default function CartRoutes() {
  return (
    <Switch>
      <AuthenticatedGuard
        exact
        path={PATH.CART}
        component={() => (
          <Suspense fallback={<Loading />}>
            <Cart />
          </Suspense>
        )}
      />
    </Switch>
  )
}
