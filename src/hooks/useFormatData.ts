import { GENDER } from "../constants/styles"
export function formatDate(date: string) {
  const dateConvert = new Date(date)
  const month = dateConvert.getMonth() + 1
  return (
    dateConvert.getFullYear() +
    "-" +
    (month < 10 ? "0" + month : month) +
    "-" +
    dateConvert.getDate()
  )
}

export function formatGender(gender: number) {
  if (gender === 1) return GENDER.MALE
  else if (gender === 2) return GENDER.FEMALE
  else return "Not selected"
}
