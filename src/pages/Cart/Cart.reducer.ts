import produce from "immer"
import * as types from "./Cart.constants"
interface listCartInterface {
  loading: boolean
  cartList: ProductCart[]
}

const initialState: listCartInterface = {
  loading: false,
  cartList: []
}

export const cartReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.GET_CART_REQUESTED:
        draft.loading = true
        break
      case types.GET_CART_SUCCESS:
        draft.loading = false
        draft.cartList = action.data.data.associateCartProduct
        break
      case types.GET_CART_FAILED:
        draft.loading = false
        break
      case types.REMOVE_SELECTED_ITEM:
        draft.cartList = state.cartList.filter(
          item => item !== state.cartList[action.index]
        )
        break
      case types.ON_ADD_ITEM:
        draft.cartList = state.cartList.map(item =>
          item.id === action.item.id
            ? { ...item, productNumber: item.productNumber + 1 }
            : item
        )
        break
      case types.ON_REMOVE_ITEM:
        draft.cartList = state.cartList.map(item =>
          item.id === action.item.id
            ? { ...item, productNumber: item.productNumber - 1 }
            : item
        )
        break
      default:
        return state
    }
  })
