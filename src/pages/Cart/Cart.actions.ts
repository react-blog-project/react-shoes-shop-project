import * as types from "./Cart.constants"

export const removeSelectedItem = (index: number) => ({
  type: types.REMOVE_SELECTED_ITEM,
  index
})

export const onAddItem = (id: number, productNumber: number) => ({
  type: types.ON_ADD_ITEM,
  item: {
    id,
    productNumber
  }
})

export const onRemoveItem = (id: number, productNumber: number) => ({
  type: types.ON_REMOVE_ITEM,
  item: {
    id,
    productNumber
  }
})

export const getCartRequested = () => ({
  type: types.GET_CART_REQUESTED
})

export const getCartSuccess = data => ({
  type: types.GET_CART_SUCCESS,
  data
})

export const getCartFailed = (errorMessage: string) => ({
  type: types.GET_CART_FAILED,
  errorMessage
})

export const updateCartRequested = () => ({
  type: types.UPDATE_CART_REQUESTED
})

export const updateCartSuccess = message => ({
  type: types.UPDATE_CART_SUCCESS,
  message
})

export const updateCartFailed = (errorMessage: string) => ({
  type: types.UPDATE_CART_FAILED,
  errorMessage
})
