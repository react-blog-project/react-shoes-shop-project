import React, { useEffect } from "react"
import { connect, ConnectedProps } from "react-redux"
import Checkout from "src/components/Checkout/Checkout"
import SEO from "src/components/SEO/SEO"
import Table from "src/components/Table/Table"
import MainLayout from "../../layouts/MainLayout/MainLayout"
import { getCartList } from "./Cart.thunk"

const mapDispatchToProps = dispatch => ({
  getCartList: () => dispatch(getCartList())
})

const connector = connect(null, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function Cart(props: Props) {
  const { getCartList } = props

  useEffect(() => {
    getCartList()
  }, [getCartList])

  return (
    <div>
      <SEO title="Cart page" description="This is cart page" />
      <MainLayout>
        <Table />
        <Checkout />
      </MainLayout>
    </div>
  )
}

export default connector(Cart)
