import { getCart, updateCart } from "src/apis/cart.api"
import * as actions from "./Cart.actions"

export const getCartList = () => dispatch => {
  dispatch(actions.getCartRequested())
  return getCart()
    .then(res => {
      dispatch(actions.getCartSuccess(res))
    })
    .catch(err => Promise.reject(dispatch(actions.getCartFailed(err))))
}

export const updateCartList = (payload: IupdateCart) => dispatch => {
  dispatch(actions.getCartRequested())
  return updateCart(payload)
    .then(res => {
      dispatch(actions.updateCartSuccess(res))
    })
    .catch(err => Promise.reject(dispatch(actions.updateCartFailed(err))))
}
