import { MouseEventHandler } from "react"
import Product from "../../../components/ProductItem/ProductItem"
import "./BestSeller.scss"
interface Props {
  listProducts: Product[]
  handleLoadMore: MouseEventHandler<HTMLDivElement>
  handleClickProduct: (slug: string) => void
}

function BestSeller(props: Props) {
  const { listProducts, handleLoadMore, handleClickProduct } = props

  return (
    <div className="flash-sale">
      <h3 className="flash-sale__title text-center pb-3 pt-2">BEST SELLER</h3>
      <div className="row g-3 mx-2 mx-md-5 g-md-4 flash-sale__content">
        {listProducts.map((product, index) => (
          <div className="col-6 col-md-3 " key={index}>
            <Product
              product={product}
              onClick={() => handleClickProduct(product.slug)}
            />
          </div>
        ))}
      </div>
      <h6 className="load-more pt-4" onClick={handleLoadMore}>
        LOAD MORE
      </h6>
    </div>
  )
}
export default BestSeller
