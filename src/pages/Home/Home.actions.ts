import * as types from "./Home.constants"

export const getProductRequested = () => ({
  type: types.GET_PRODUCT_REQUEST
})

export const getProductSuccess = (listProducts: Product[]) => ({
  type: types.GET_PRODUCT_SUCCESS,
  listProducts: listProducts
})

export const getProductFailed = (errorMessage: string) => ({
  type: types.GET_PRODUCT_FAIL,
  error: errorMessage
})

export const getMoreProductSuccess = (listProducts: Product[]) => ({
  type: types.GET_MORE_PRODUCT_SUCCESS,
  listProducts: listProducts
})
