import { Swiper, SwiperSlide } from "swiper/react"
import "swiper/swiper.min.css"
import "swiper/components/pagination/pagination.min.css"
import SwiperCore, { Pagination } from "swiper/core"
import CarouselItem from "./CarouselItem"
import "./Carousel.scss"

SwiperCore.use([Pagination])

function Carousel() {
  const items = [1, 2, 3, 4, 5]
  return (
    <Swiper
      pagination={{
        clickable: true
      }}
      className="mySwiper"
    >
      {items.map(item => (
        <SwiperSlide key={item} className="mySwiper__item">
          <CarouselItem
            image={{
              url: "https://deestore.vn/wp-content/uploads/2019/07/cong-nghe-bounce-1140x800.jpg",
              alt: "shoe one"
            }}
            title="Super Flash Sale 50% Off "
            time={{
              hour: "08",
              minute: "34",
              seconds: "52"
            }}
          />
        </SwiperSlide>
      ))}
    </Swiper>
  )
}
export default Carousel
