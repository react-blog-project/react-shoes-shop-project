import React from "react"
import "./CarouselItem.scss"

type CarouselProps = {
  image: {
    url: string
    alt: string
  }
  title: string
  time?: {
    hour: string
    minute: string
    seconds: string
  }
}

export default function CarouselItem(props: CarouselProps) {
  const {
    image = {
      url: "",
      alt: ""
    },
    title,
    time = {
      hour: "",
      minute: "",
      seconds: ""
    }
  } = props
  return (
    <div className="carousel__item">
      <p className="title">{title}</p>
      <div className="time">
        <span className="time__content">{time.hour}</span>
        <span className="time__space">:</span>
        <span className="time__content">{time.minute}</span>
        <span className="time__space">:</span>
        <span className="time__content">{time.seconds}</span>
      </div>
      <img src={image.url} alt={image.url} />
    </div>
  )
}
