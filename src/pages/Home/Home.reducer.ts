import * as types from "./Home.constants"
import produce from "immer"

interface listProductsInterFace {
  loading: boolean
  error: string
  listProducts: Product[]
}

const initialState: listProductsInterFace = {
  loading: false,
  error: "",
  listProducts: []
}

export const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.GET_MORE_PRODUCT_SUCCESS:
        draft.loading = false
        draft.listProducts = state.listProducts.concat(action.listProducts)
        break
      case types.GET_PRODUCT_REQUEST:
        draft.loading = true
        break
      case types.GET_PRODUCT_SUCCESS:
        draft.loading = false
        draft.listProducts = action.listProducts
        break
      case types.GET_PRODUCT_FAIL:
        draft.loading = false
        draft.error = action.error
        break
      default:
        return state
    }
  })
