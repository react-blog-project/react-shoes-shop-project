import { useEffect, useState } from "react"
import { connect, ConnectedProps } from "react-redux"
import { useHistory } from "react-router-dom"
import MainLayout from "../../layouts/MainLayout/MainLayout"
import SEO from "../../components/SEO/SEO"
import BestSeller from "./BestSeller/BestSeller"
import Carousel from "./Carousel/Carousel"
import Banner from "./Banner/Banner"
import InformationPayment from "./InformationPayment/InformationPayment"
import PageLoader from "../../components/Loading/PageLoader"
import { PATH } from "../../constants/index"
import { getMoreProduct, getProductList } from "./Home.thunk"

const mapStateToProps = (state: AppState) => ({
  loading: state.home.loading,
  error: state.home.error,
  listProducts: state.home.listProducts
})

const mapDispatchToProps = dispatch => ({
  getProductList: (page: number) => dispatch(getProductList(page)),
  getMoreProduct: (page: number) => dispatch(getMoreProduct(page))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function Home(props: Props) {
  const { getMoreProduct, getProductList, loading, listProducts } = props
  const history = useHistory()

  const handleClickProduct = (slug: string) => {
    history.push(`${PATH.PRODUCT}/${slug}`)
  }

  const [page, setPage] = useState(0)

  const handleLoadMore = () => {
    setPage(page + 1)
    getMoreProduct(page + 1)
  }

  useEffect(() => {
    if (!listProducts.length) getProductList(0)
  }, [getProductList, listProducts.length])

  return (
    <div>
      <SEO title="Home page" description="This is home page" />
      <MainLayout>
        <Carousel />
        <BestSeller
          listProducts={listProducts}
          handleClickProduct={handleClickProduct}
          handleLoadMore={handleLoadMore}
        />
        <Banner />
        <InformationPayment />
      </MainLayout>
      {loading && <PageLoader />}
    </div>
  )
}
export default connector(Home)
