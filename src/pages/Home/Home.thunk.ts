import { getProductListApi } from "../../apis/product.api"
import * as actionHome from "./Home.actions"

export const getProductList = (page: number) => dispatch => {
  dispatch(actionHome.getProductRequested())
  return getProductListApi(page)
    .then(res => {
      dispatch(actionHome.getProductSuccess(res.data.products))
    })
    .catch(err =>
      Promise.reject(dispatch(actionHome.getProductFailed(err.message)))
    )
}

export const getMoreProduct = (page: number) => dispatch => {
  dispatch(actionHome.getProductRequested())
  return getProductListApi(page)
    .then(res => {
      dispatch(actionHome.getMoreProductSuccess(res.data.products))
    })
    .catch(err =>
      Promise.reject(dispatch(actionHome.getProductFailed(err.message)))
    )
}
