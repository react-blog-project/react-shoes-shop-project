import { ReactNode } from "react"
import { ReactComponent as Shipping } from "../../../assets/images/shipping.svg"
import { ReactComponent as Refund } from "../../../assets/images/refund.svg"
import { ReactComponent as Support } from "../../../assets/images/support.svg"
import "./InformationPayment.scss"

type ItemPaymentProps = {
  children: ReactNode
  title: string
  content: string
}

function ItemPayment({ children, title, content }: ItemPaymentProps) {
  return (
    <div className="item-payment mx-md-5 mx-2 d-flex flex-column align-items-center">
      {children}
      <p className="item-payment__title">{title}</p>
      <p className="item-payment__content">{content}</p>
    </div>
  )
}

function InformationPayment() {
  return (
    <div className="information-payment row">
      <div className="col-md-4">
        <ItemPayment
          title="FREE SHIPPING"
          content="Lorem Ipsum is simply dummy text of 
                the printing and typesetting industry."
        >
          <Shipping className="icon" />
        </ItemPayment>
      </div>
      <div className="col-md-4">
        <ItemPayment
          title="100% REFUND"
          content="Lorem Ipsum is simply dummy text of 
                the printing and typesetting industry."
        >
          <Refund className="icon" />
        </ItemPayment>
      </div>
      <div className="col-md-4">
        <ItemPayment
          title="SUPPORT 24/7"
          content="Lorem Ipsum is simply dummy text of 
                the printing and typesetting industry."
        >
          <Support className="icon" />
        </ItemPayment>
      </div>
    </div>
  )
}

export default InformationPayment
