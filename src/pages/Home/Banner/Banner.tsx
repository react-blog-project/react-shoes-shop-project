import IMGBANNER from "../../../assets/images/img_banner.png"
import "./Banner.scss"

function Banner() {
  return (
    <div className="banner">
      <div className="banner__content col-md-6">
        <h3 className="mb-4">Adidas Men Running Sneakers</h3>
        <p className="banner__content--title">
          Performance and design. Taken right to the edge.
        </p>
        <p className="banner__content--information">SHOP NOW</p>
        <div className="hr"></div>
      </div>
      <div className="col-md-5">
        <img src={IMGBANNER} alt="img banner" />
      </div>
    </div>
  )
}

export default Banner
