import * as types from "./AddAddress.constants"
import produce from "immer"

interface addAddressInterface {
  loading: boolean
  listProvince: any
  province: string
  provinceId: number
  listDistrict: any
  district: string
  districtId: number
  listWard: any
  ward: string
  errorMessage: string
  streetAddress: string
  streetAddress2: string
}

const initialState: addAddressInterface = {
  loading: false,
  listProvince: [],
  province: "",
  provinceId: 0,
  listDistrict: [],
  district: "",
  districtId: 0,
  listWard: [],
  ward: "",
  errorMessage: "",
  streetAddress: "",
  streetAddress2: ""
}

export const addAddressReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.GET_PROVINCE_REQUESTED:
        draft.loading = true
        break
      case types.GET_PROVINCE_SUCCESS:
        draft.listProvince = action.listProvince
        draft.errorMessage = ""
        draft.loading = false
        break
      case types.GET_PROVINCE_FAILED:
        draft.errorMessage = action.errorMessage
        draft.loading = false
        break
      case types.CHANGE_PROVINCE:
        const provinceSelect = state.listProvince.filter(
          item => item.name === action.province
        )[0]
        draft.errorMessage = ""
        draft.province = action.province
        draft.provinceId = provinceSelect.code
        draft.listDistrict = []
        draft.district = ""
        draft.listWard = []
        draft.ward = ""
        break

      // ===========================> District
      case types.GET_DISTRICT_REQUESTED:
        draft.loading = true
        break
      case types.GET_DISTRICT_SUCCESS:
        draft.listDistrict = action.listDistrict
        draft.errorMessage = ""
        draft.loading = false
        break
      case types.GET_DISTRICT_FAILED:
        draft.errorMessage = action.errorMessage
        draft.loading = false
        break
      case types.CHANGE_DISTRICT:
        const districtSelect = state.listDistrict.filter(
          item => item.name === action.district
        )[0]
        draft.errorMessage = ""
        draft.district = action.district
        draft.districtId = districtSelect.code
        draft.listWard = []
        draft.ward = ""
        break

      // ===========================> Ward
      case types.GET_WARD_REQUESTED:
        draft.loading = true
        break
      case types.GET_WARD_SUCCESS:
        draft.listWard = action.listWard
        draft.errorMessage = ""
        draft.loading = false
        break
      case types.GET_WARD_FAILED:
        draft.errorMessage = action.errorMessage
        draft.loading = false
        break
      case types.CHANGE_WARD:
        draft.errorMessage = ""
        draft.ward = action.ward
        break

      // ===========================> Street
      case types.CHANGE_STREET_ADDRESS:
        draft.streetAddress = action.streetAddress
        break
      case types.CHANGE_STREET_ADDRESS2:
        draft.streetAddress2 = action.streetAddress2
        break
      case types.ADD_ADDRESS_REQUESTED:
        draft.loading = true
        break
      case types.ADD_ADDRESS_SUCCESS:
        draft.loading = false
        draft.ward = ""
        draft.errorMessage = ""
        draft.streetAddress = ""
        draft.streetAddress2 = ""
        draft.province = ""
        draft.district = ""
        break
      case types.ADD_ADDRESS_FAILED:
        draft.loading = false
        draft.errorMessage = action.message
        break
      default:
        return state
    }
  })
