import {
  getProvinces,
  getDistricts,
  getWards,
  postAddress
} from "../../../apis/address.api"
import * as actions from "./AddAddress.actions"

export const doGetProvinces = () => dispatch => {
  dispatch(actions.getProvincesRequested())
  return getProvinces()
    .then(res => dispatch(actions.getProvincesSuccess(res.data)))
    .catch(err => Promise.reject(dispatch(actions.getProvincesFailed(err))))
}

export const doGetDistricts = (payload: number) => dispatch => {
  dispatch(actions.getsDistrictsRequested())
  return getDistricts(payload)
    .then(res => dispatch(actions.getDistrictsSuccess(res.data.districts)))
    .catch(err => Promise.reject(dispatch(actions.getDistrictsFailed(err))))
}

export const doGetWards = (payload: number) => dispatch => {
  dispatch(actions.getsWardsRequested())
  return getWards(payload)
    .then(res => dispatch(actions.getWardsSuccess(res.data.wards)))
    .catch(err => Promise.reject(dispatch(actions.getWardsFailed(err))))
}

export const createNewAddress = (address: Address) => dispatch => {
  dispatch(actions.addAddressRequested())
  return postAddress(address)
    .then(res =>
      dispatch(actions.addAddressSuccess({ ...address, id: res.data.id }))
    )
    .catch(err => Promise.reject(dispatch(actions.addAddressFailed(err))))
}
