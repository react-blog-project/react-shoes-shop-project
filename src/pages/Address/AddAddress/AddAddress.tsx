import { yupResolver } from "@hookform/resolvers/yup"
import { useState } from "react"
import { useForm } from "react-hook-form"
import { connect, ConnectedProps } from "react-redux"
import { useHistory } from "react-router-dom"
import * as yup from "yup"
import Button from "../../../components/Button/Button"
import Dropdown from "../../../components/Dropdown/Dropdown"
import Input from "../../../components/Input/Input"
import PageLoader from "../../../components/Loading/PageLoader"
import { PATH } from "../../../constants"
import AccountLayout from "../../../layouts/AccountLayout/AccountLayout"

import * as actionValidations from "../../../constants/validation"
import {
  doGetDistricts,
  doGetProvinces,
  doGetWards,
  createNewAddress
} from "./AddAddress.thunks"
import * as actions from "./AddAddress.actions"
import "./AddAddress.scss"

const mapStateToProps = (state: AppState) => ({
  loading: state.addAddress.loading,
  listProvince: state.addAddress.listProvince,
  province: state.addAddress.province,
  provinceId: state.addAddress.provinceId,
  listDistrict: state.addAddress.listDistrict,
  district: state.addAddress.district,
  districtId: state.addAddress.districtId,
  listWard: state.addAddress.listWard,
  ward: state.addAddress.ward,
  streetAddress: state.addAddress.streetAddress,
  streetAddress2: state.addAddress.streetAddress2,
  errorMessage: state.addAddress.errorMessage
})

const mapDispatchToProps = dispatch => ({
  doGetProvinces: () => dispatch(doGetProvinces()),
  doGetDistricts: (province: number) => dispatch(doGetDistricts(province)),
  doGetWards: (district: number) => dispatch(doGetWards(district)),
  createNewAddress: (address: Address) => dispatch(createNewAddress(address)),
  setProvince: (province: string) => dispatch(actions.setProvince(province)),
  setDistrict: (district: string) => dispatch(actions.setDistrict(district)),
  setWard: (ward: string) => dispatch(actions.setWard(ward)),
  setStreetAddress: (streetAddress: string) =>
    dispatch(actions.setStreetAddress(streetAddress)),
  setStreetAddress2: (streetAddress2: string) =>
    dispatch(actions.setStreetAddress2(streetAddress2))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function AddAddress(props: Props) {
  const {
    loading,
    listProvince,
    province,
    provinceId,
    listDistrict,
    district,
    districtId,
    listWard,
    ward,
    streetAddress,
    streetAddress2,
    errorMessage,
    doGetProvinces,
    doGetDistricts,
    doGetWards,
    setProvince,
    setDistrict,
    setWard,
    setStreetAddress,
    setStreetAddress2,
    createNewAddress
  } = props
  const history = useHistory()

  const [provinceErr, setProvinceErr] = useState("")

  const onProvinceClick = () => {
    if (listProvince.length !== 0) return
    doGetProvinces()
  }
  const handleProvince = (value: string) => {
    setProvince(value)
  }

  const onDistrictCLick = () => {
    if (listDistrict.length !== 0) return
    doGetDistricts(provinceId)
  }
  const handleDistrict = (value: string) => {
    setDistrict(value)
  }

  const onWardCLick = () => {
    if (listWard.length !== 0) return
    doGetWards(districtId)
  }
  const handleWard = (value: string) => {
    setWard(value)
  }

  const handleAddressStreet = event => {
    register("street").onChange(event)
    setStreetAddress(event.target.value)
  }
  const handleAddressStreet2 = event => {
    register("streetAddress2").onChange(event)
    setStreetAddress2(event.target.value)
  }

  const submit = (e: React.FormEvent) => {
    e.preventDefault()
    if (!province || !district || !ward || !streetAddress) {
      setProvinceErr(actionValidations.REQUIRE)
      setError("street", {
        type: "onChange",
        message: actionValidations.REQUIRE
      })
    } else {
      createNewAddress({
        street: streetAddress,
        streetAddress2,
        province,
        district,
        ward
      }).then(() => history.push(PATH.ADDRESS))
    }
  }

  const schema = yup.object().shape({
    street: yup.string().required(actionValidations.REQUIRE),
    streetAddress2: yup.string()
  })

  const {
    register,
    setError,
    formState: { errors }
  } = useForm<Address>({
    mode: "onChange",
    resolver: yupResolver(schema)
  })

  return (
    <AccountLayout title="Add Address" pathBack="address">
      <form method="post" onSubmit={submit} className="form-address px-3 pt-3">
        <Dropdown
          option={listProvince.map(item => item.name)}
          onChange={handleProvince}
          onClick={onProvinceClick}
          label="Choose Province"
          value={province ? province : "Province..."}
          errorMessage={errorMessage}
          errors={province ? "" : provinceErr}
        />
        <Dropdown
          option={listDistrict.map(item => item.name)}
          onChange={handleDistrict}
          onClick={onDistrictCLick}
          label="Choose District"
          value={district ? district : "District..."}
          errorMessage={errorMessage}
          errors={district ? "" : provinceErr}
        />
        <Dropdown
          option={listWard.map(item => item.name)}
          onChange={handleWard}
          onClick={onWardCLick}
          label="Choose Ward"
          value={ward ? ward : "Ward..."}
          errorMessage={errorMessage}
          errors={ward ? "" : provinceErr}
        />
        <Input
          name="streetAddress"
          registerInput={register}
          type="text"
          placeholder="Street Address"
          className="input-address"
          label="Street Address"
          errorMessage={errorMessage}
          errors={errors.street}
          onChange={handleAddressStreet}
        />
        <Input
          name="streetAddress2"
          registerInput={register}
          type="text"
          placeholder="Street Address 2"
          className="input-address"
          label="Street Address 2(Optional)"
          errors={errors.streetAddress2}
          onChange={handleAddressStreet2}
        />
        {errorMessage && <p className="error">{errorMessage}</p>}
        <Button
          type="submit"
          className="add-address--button mt-4"
          value="Add Address"
        />
      </form>
      {loading && <PageLoader />}
    </AccountLayout>
  )
}

export default connector(AddAddress)
