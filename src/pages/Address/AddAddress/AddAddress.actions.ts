import * as types from "./AddAddress.constants"

export const addAddressRequested = () => ({
  type: types.ADD_ADDRESS_REQUESTED
})

export const addAddressSuccess = (address: Address) => ({
  type: types.ADD_ADDRESS_SUCCESS,
  address
})

export const addAddressFailed = (message: string) => ({
  type: types.ADD_ADDRESS_FAILED,
  message
})

export const getProvincesRequested = () => ({
  type: types.GET_PROVINCE_REQUESTED
})

export const getProvincesSuccess = (listProvince: []) => ({
  type: types.GET_PROVINCE_SUCCESS,
  listProvince
})

export const getProvincesFailed = (message: string) => ({
  type: types.GET_PROVINCE_FAILED,
  message
})

export const setProvince = (province: string) => ({
  type: types.CHANGE_PROVINCE,
  province
})

// ===============================> District
export const getsDistrictsRequested = () => ({
  type: types.GET_PROVINCE_REQUESTED
})

export const getDistrictsSuccess = (listDistrict: []) => ({
  type: types.GET_DISTRICT_SUCCESS,
  listDistrict
})

export const getDistrictsFailed = (message: string) => ({
  type: types.GET_DISTRICT_FAILED,
  message
})

export const setDistrict = (district: string) => ({
  type: types.CHANGE_DISTRICT,
  district
})

// ===============================> Ward
export const getsWardsRequested = () => ({
  type: types.GET_WARD_REQUESTED
})

export const getWardsSuccess = (listWard: []) => ({
  type: types.GET_WARD_SUCCESS,
  listWard
})

export const getWardsFailed = (message: string) => ({
  type: types.GET_WARD_FAILED,
  message
})

export const setWard = (ward: string) => ({
  type: types.CHANGE_WARD,
  ward
})

export const setStreetAddress = (streetAddress: string) => ({
  type: types.CHANGE_STREET_ADDRESS,
  streetAddress
})

export const setStreetAddress2 = (streetAddress2: string) => ({
  type: types.CHANGE_STREET_ADDRESS2,
  streetAddress2
})
