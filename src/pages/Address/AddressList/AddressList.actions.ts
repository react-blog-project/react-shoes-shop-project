import * as types from "./AddressList.constants"

export const deleteAddressRequest = () => ({
  type: types.DELETE_ADDRESS_REQUEST
})

export const deleteAddressSuccess = (id: number) => ({
  type: types.DELETE_ADDRESS_SUCCESS,
  id
})

export const deleteAddressFailed = (errorMessage: string) => ({
  type: types.DELETE_ADDRESS_FAILED,
  errorMessage
})
