import * as typesAddAddress from "../AddAddress/AddAddress.constants"
import * as typesApp from "../../../App/App.constants"
import * as types from "./AddressList.constants"
import produce from "immer"

interface changePasswordInterface {
  loading: boolean
  errorMessage: string
  listAddress: Address[]
}

const initialState: changePasswordInterface = {
  loading: false,
  errorMessage: "",
  listAddress: []
}

export const listAddressReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case typesApp.USER_REQUEST_INFO_SUCCESS:
        draft.listAddress = action.user.address
        break
      case typesAddAddress.ADD_ADDRESS_SUCCESS:
        draft.listAddress.push(action.address)
        draft.loading = false
        break
      case types.DELETE_ADDRESS_REQUEST:
        draft.loading = true
        break
      case types.DELETE_ADDRESS_SUCCESS:
        draft.listAddress = state.listAddress.filter(
          address => address.id !== action.id
        )
        draft.loading = false
        break
      case types.DELETE_ADDRESS_FAILED:
        draft.errorMessage = action.errorMessage
        draft.loading = false
        break
      default:
        return state
    }
  })
