import { deleteAddressApi } from "../../../apis/address.api"
import * as actions from "./AddressList.actions"

export const deleteAddress = (id: number) => dispatch => {
  dispatch(actions.deleteAddressRequest())
  return deleteAddressApi(id)
    .then(res => dispatch(actions.deleteAddressSuccess(id)))
    .catch(err => Promise.reject(dispatch(actions.deleteAddressFailed(err))))
}
