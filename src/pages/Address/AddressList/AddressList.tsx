import { connect, ConnectedProps } from "react-redux"
import { Link } from "react-router-dom"
import AddressItem from "../../../components/AddressItem/AddressItem"
import Button from "../../../components/Button/Button"
import AccountLayout from "../../../layouts/AccountLayout/AccountLayout"
import { deleteAddress } from "./AddressList.thunks"
import "./AddressList.scss"

const mapStateToProps = (state: AppState) => ({
  user: state.app.user,
  listAddress: state.listAddress.listAddress
})

const mapDispatchToProps = dispatch => ({
  deleteAddress: (id: number) => dispatch(deleteAddress(id))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function AddressList(props: Props) {
  const { user, listAddress, deleteAddress } = props

  const handleName = user.firstName + " " + user.lastName

  const onCancel = () => {}
  const onDelete = (id: number) => {
    deleteAddress(id)
  }

  return (
    <AccountLayout title="Address" pathBack="account">
      <div className="px-3 mt-3">
        {listAddress.map((item, index) => (
          <AddressItem
            name={handleName}
            mobile={user.mobile}
            address={item}
            key={index}
            onCancel={onCancel}
            onDelete={() => onDelete(item.id)}
          />
        ))}
        <Link to="/add-address">
          <Button
            type="button"
            value="Add Address"
            className="address__button"
          />
        </Link>
      </div>
    </AccountLayout>
  )
}

export default connector(AddressList)
