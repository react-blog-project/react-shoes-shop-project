import * as types from "./ChangePassword.constants"
import * as typeValidations from "../../constants/validation"

export const setOldPassword = (oldPassword: string) => ({
  type: types.CHANGE_OLD_PASSWORD,
  oldPassword
})

export const setNewPassword = (newPassword: string) => ({
  type: types.CHANGE_NEW_PASSWORD,
  newPassword
})

export const setPasswordConfirm = (passwordConfirm: string) => ({
  type: typeValidations.CHANGE_PASSWORD_CONFIRM,
  passwordConfirm
})

export const setErrorMessage = (errorMessage: string) => ({
  type: typeValidations.CHANGE_ERROR,
  errorMessage
})

export const changePasswordRequested = () => ({
  type: types.CHANGE_PASSWORD_REQUESTED
})

export const changePasswordSuccess = () => ({
  type: types.CHANGE_PASSWORD_SUCCESS
})

export const changePasswordFailed = (message: string) => ({
  type: types.CHANGE_PASSWORD_FAILED,
  message
})
