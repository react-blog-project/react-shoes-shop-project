import { patchChangePassword } from "../../apis/user.api"
import * as actions from "./ChangePassword.actions"

export const doChangePassword =
  (payload: RequestChangePassword) => dispatch => {
    dispatch(actions.changePasswordRequested())
    return patchChangePassword(payload)
      .then(() => dispatch(actions.changePasswordSuccess()))
      .catch(err => Promise.reject(dispatch(actions.changePasswordFailed(err))))
  }
