import { useForm } from "react-hook-form"
import { connect, ConnectedProps } from "react-redux"
import { useHistory, Link } from "react-router-dom"
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import PublicLayout from "../../layouts/PublicLayout/PublicLayout"
import Input from "../../components/Input/Input"
import Button from "../../components/Button/Button"
import SEO from "../../components/SEO/SEO"
import { ReactComponent as PasswordIcon } from "../../assets/images/password.svg"
import { PATH } from "../../constants"
import { doChangePassword } from "./ChangePassword.thunks"
import * as actionValidations from "../../constants/validation"
import * as actions from "./ChangePassword.actions"
import * as types from "./ChangePassword.constants"
import "./ChangePassword.scss"

const mapStateToProps = (state: AppState) => ({
  loading: state.changePassword.loading,
  oldPassword: state.changePassword.oldPassword,
  newPassword: state.changePassword.newPassword,
  passwordConfirm: state.changePassword.passwordConfirm,
  errorMessage: state.changePassword.errorMessage
})

const mapDispatchToProps = dispatch => ({
  doChangePassword: (params: RequestChangePassword) =>
    dispatch(doChangePassword(params)),
  setOldPassword: (oldPassword: string) =>
    dispatch(actions.setOldPassword(oldPassword)),
  setNewPassword: (newPassword: string) =>
    dispatch(actions.setNewPassword(newPassword)),
  setPasswordConfirm: (passwordConfirm: string) =>
    dispatch(actions.setPasswordConfirm(passwordConfirm)),
  setErrorMessage: (message: string) =>
    dispatch(actions.setErrorMessage(message))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

const ChangePassword = (props: Props) => {
  const {
    doChangePassword,
    setOldPassword,
    setNewPassword,
    setPasswordConfirm,
    setErrorMessage,
    loading,
    oldPassword,
    newPassword,
    passwordConfirm,
    errorMessage
  } = props

  const history = useHistory()

  const handleOldPassword = event => {
    register("oldPassword").onChange(event)
    setOldPassword(event.target.value)
  }

  const handleNewPassword = event => {
    register("newPassword").onChange(event)
    setNewPassword(event.target.value)
  }

  const handlePasswordConfirm = event => {
    register("passwordConfirm").onChange(event)
    setPasswordConfirm(event.target.value)
  }

  const comparePassword = () => {
    if (oldPassword === newPassword) return 1
    if (newPassword !== passwordConfirm) return 2
    return 0
  }

  const submit = () => {
    if (!loading && comparePassword() === 0) {
      doChangePassword({ oldPassword, newPassword, passwordConfirm }).then(
        () => {
          history.push(PATH.HOME)
        }
      )
    } else if (comparePassword() === 1) setErrorMessage(types.SAME_PASSWORD)
    else setErrorMessage(types.CONFIRM_PASSWORD)
  }

  const schema = yup.object().shape({
    oldPassword: yup
      .string()
      .min(
        actionValidations.PASSWORD_MIN_LENGTH,
        actionValidations.MIN_PASSWORD
      )
      .required(actionValidations.REQUIRE),
    newPassword: yup
      .string()
      .min(
        actionValidations.PASSWORD_MIN_LENGTH,
        actionValidations.MIN_PASSWORD
      )
      .required(actionValidations.REQUIRE),
    passwordConfirm: yup
      .string()
      .min(
        actionValidations.PASSWORD_MIN_LENGTH,
        actionValidations.MIN_PASSWORD
      )
      .required(actionValidations.REQUIRE)
  })

  const {
    register,
    formState: { errors }
  } = useForm<RequestChangePassword>({
    mode: "onChange",
    resolver: yupResolver(schema)
  })
  return (
    <div>
      <SEO title="Change Password" description="This is change password page" />
      <PublicLayout
        title="Change Password"
        subTitle="Use a strong password"
        classStyle="register"
        loading={loading}
      >
        <div className="form">
          <Input
            name="oldPassword"
            registerInput={register}
            type="password"
            placeholder="Old Password"
            errorMessage={errorMessage}
            errors={errors.oldPassword}
            onChange={handleOldPassword}
          >
            <PasswordIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          <Input
            name="newPassword"
            registerInput={register}
            type="password"
            placeholder="New Password"
            errorMessage={errorMessage}
            errors={errors.newPassword}
            onChange={handleNewPassword}
          >
            <PasswordIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          <Input
            name="passwordConfirm"
            registerInput={register}
            type="password"
            placeholder="Password Again"
            errorMessage={errorMessage}
            errors={errors.passwordConfirm}
            onChange={handlePasswordConfirm}
          >
            <PasswordIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          {errorMessage && <p className="error">{errorMessage}</p>}
          <Button
            className="px-3 my-2"
            type="button"
            value="Change Password"
            onClick={submit}
          />
        </div>
        <footer className="text-center">
          <p>
            return home ?
            <span>
              <Link to={PATH.HOME} className="text__color">
                Home
              </Link>
            </span>
          </p>
        </footer>
      </PublicLayout>
    </div>
  )
}

export default connector(ChangePassword)
