import * as types from "./ChangePassword.constants"
import * as typeValidations from "../../constants/validation"
import produce from "immer"

interface changePasswordInterface {
  loading: boolean
  oldPassword: string
  newPassword: string
  passwordConfirm: string
  errorMessage: string
}
const initialState: changePasswordInterface = {
  loading: false,
  oldPassword: "",
  newPassword: "",
  passwordConfirm: "",
  errorMessage: ""
}

export const changePasswordReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.CHANGE_OLD_PASSWORD:
        draft.errorMessage = ""
        draft.oldPassword = action.oldPassword
        break
      case types.CHANGE_NEW_PASSWORD:
        draft.errorMessage = ""
        draft.newPassword = action.newPassword
        break
      case typeValidations.CHANGE_PASSWORD_CONFIRM:
        draft.errorMessage = ""
        draft.passwordConfirm = action.passwordConfirm
        break
      case typeValidations.CHANGE_ERROR:
        draft.errorMessage = action.errorMessage
        break
      case types.CHANGE_PASSWORD_REQUESTED:
        draft.loading = true
        break
      case types.CHANGE_PASSWORD_SUCCESS:
        draft.errorMessage = ""
        draft.loading = false
        break
      case types.CHANGE_PASSWORD_FAILED:
        draft.errorMessage = action.errorMessage
        draft.loading = false
        break
      default:
        return state
    }
  })
