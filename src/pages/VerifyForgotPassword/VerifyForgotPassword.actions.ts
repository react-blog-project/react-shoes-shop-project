import * as types from "./VerifyForgotPassword.constants"
import * as typeValidations from "../../constants/validation"

export const setPassword = (password: string) => ({
  type: typeValidations.CHANGE_PASSWORD,
  password
})

export const setPasswordConfirm = (passwordConfirm: string) => ({
  type: typeValidations.CHANGE_PASSWORD_CONFIRM,
  passwordConfirm
})

export const setErrorMessage = (errorMessage: string) => ({
  type: typeValidations.CHANGE_ERROR,
  errorMessage
})

export const verifyForgotPasswordRequested = () => ({
  type: types.VERIFY_FORGOT_PASSWORD_REQUESTED
})

export const verifyForgotPasswordSuccess = () => ({
  type: types.VERIFY_FORGOT_PASSWORD_SUCCESS
})

export const verifyForgotPasswordFailed = (errorMessage: string) => ({
  type: types.VERIFY_FORGOT_PASSWORD_FAILED,
  errorMessage
})
