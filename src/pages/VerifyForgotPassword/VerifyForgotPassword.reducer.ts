import * as types from "./VerifyForgotPassword.constants"
import * as typeValidations from "../../constants/validation"
import produce from "immer"

interface verifyForgotPasswordStateInterFace {
  loading: boolean
  password: string
  passwordConfirm: string
  errorMessage: string
}

const initialState: verifyForgotPasswordStateInterFace = {
  loading: false,
  password: "",
  passwordConfirm: "",
  errorMessage: ""
}

export const VerifyForgotPasswordReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case typeValidations.CHANGE_PASSWORD:
        draft.errorMessage = ""
        draft.password = action.password
        break
      case typeValidations.CHANGE_PASSWORD_CONFIRM:
        draft.errorMessage = ""
        draft.passwordConfirm = action.passwordConfirm
        break
      case typeValidations.CHANGE_ERROR:
        draft.errorMessage = action.errorMessage
        break

      case types.VERIFY_FORGOT_PASSWORD_REQUESTED:
        draft.loading = true
        break
      case types.VERIFY_FORGOT_PASSWORD_SUCCESS:
        draft.loading = false
        draft.errorMessage = ""
        break
      case types.VERIFY_FORGOT_PASSWORD_FAILED:
        draft.loading = false
        draft.errorMessage = action.errorMessage
        break
      default:
        return state
    }
  })
