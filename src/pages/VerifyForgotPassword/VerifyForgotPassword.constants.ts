export const VERIFY_FORGOT_PASSWORD_REQUESTED =
  "VERIFY_FORGOT_PASSWORD_REQUESTED"
export const VERIFY_FORGOT_PASSWORD_SUCCESS = "VERIFY_FORGOT_PASSWORD_SUCCESS"
export const VERIFY_FORGOT_PASSWORD_FAILED = "VERIFY_FORGOT_PASSWORD_FAILED"
