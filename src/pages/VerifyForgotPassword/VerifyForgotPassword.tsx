import { FormEvent } from "react"
import { useForm } from "react-hook-form"
import { connect, ConnectedProps } from "react-redux"
import { useHistory, useLocation } from "react-router-dom"
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import PublicLayout from "../../layouts/PublicLayout/PublicLayout"
import Input from "../../components/Input/Input"
import Button from "../../components/Button/Button"
import SEO from "../../components/SEO/SEO"
import { ReactComponent as PasswordIcon } from "../../assets/images/password.svg"
import { PATH } from "../../constants"
import { doVerifyForgotPassword } from "./VerifyForgotPassword.thunks"
import * as actionValidations from "../../constants/validation"
import * as actions from "./VerifyForgotPassword.actions"
import "./VerifyForgotPassword.scss"

const mapStateToProps = (state: AppState) => ({
  loading: state.verifyForgotPassword.loading,
  password: state.verifyForgotPassword.password,
  passwordConfirm: state.verifyForgotPassword.passwordConfirm,
  errorMessage: state.verifyForgotPassword.errorMessage
})

const mapDispatchToProps = dispatch => ({
  doVerifyForgotPassword: (params: RequestVerifyForgotPassword) =>
    dispatch(doVerifyForgotPassword(params)),
  setPassword: (password: string) => dispatch(actions.setPassword(password)),
  setPasswordConfirm: (passwordConfirm: string) =>
    dispatch(actions.setPasswordConfirm(passwordConfirm)),
  setErrorMessage: (message: string) =>
    dispatch(actions.setErrorMessage(message))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function VerifyForgotPassword(props: Props) {
  const {
    doVerifyForgotPassword,
    setPassword,
    setPasswordConfirm,
    setErrorMessage,
    loading,
    passwordConfirm,
    password,
    errorMessage
  } = props
  const history = useHistory()
  const token = new URLSearchParams(useLocation().search).get("token")

  const handlePassword = event => {
    register("password").onChange(event)
    setPassword(event.target.value)
  }

  const handlePasswordConfirm = event => {
    register("passwordConfirm").onChange(event)
    setPasswordConfirm(event.target.value)
  }

  const submit = (e: FormEvent) => {
    e.preventDefault()
    if (!loading && password === passwordConfirm) {
      doVerifyForgotPassword({ password, passwordConfirm, token }).then(() => {
        history.push(PATH.HOME)
      })
    } else {
      setErrorMessage(actionValidations.WRONG_PASSWORD)
    }
  }

  const schema = yup.object().shape({
    password: yup
      .string()
      .min(
        actionValidations.PASSWORD_MIN_LENGTH,
        actionValidations.MIN_PASSWORD
      )
      .required(actionValidations.REQUIRE),
    passwordConfirm: yup
      .string()
      .min(
        actionValidations.PASSWORD_MIN_LENGTH,
        actionValidations.MIN_PASSWORD
      )
      .required(actionValidations.REQUIRE)
  })
  const {
    register,
    formState: { errors }
  } = useForm<RequestVerifyForgotPassword>({
    mode: "onChange",
    resolver: yupResolver(schema)
  })

  return (
    <div>
      <SEO
        title="Verify forgot password"
        description="This is verify forgot password page"
      />
      <PublicLayout
        title="Welcome to E-com"
        subTitle="Change password to continue"
        classStyle="login"
        loading={loading}
      >
        <form method="post" onSubmit={submit}>
          <Input
            name="password"
            registerInput={register}
            type="password"
            placeholder="Password"
            errorMessage={errorMessage}
            errors={errors.password}
            onChange={handlePassword}
          >
            <PasswordIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          <Input
            name="passwordConfirm"
            registerInput={register}
            type="password"
            placeholder="Password Again"
            errorMessage={errorMessage}
            errors={errors.passwordConfirm}
            onChange={handlePasswordConfirm}
          >
            <PasswordIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          {errorMessage && <p className="error">{errorMessage}</p>}
          <Button
            className="px-3 my-2"
            type="submit"
            value="Change password"
            disabled={!!errorMessage}
          />
        </form>
      </PublicLayout>
    </div>
  )
}

export default connector(VerifyForgotPassword)
