import { postVerifyForgotPassword } from "../../apis/user.api"
import * as actions from "./VerifyForgotPassword.actions"

export const doVerifyForgotPassword =
  (payload: RequestVerifyForgotPassword) => dispatch => {
    dispatch(actions.verifyForgotPasswordRequested())

    return postVerifyForgotPassword(payload)
      .then(() => dispatch(actions.verifyForgotPasswordSuccess()))
      .catch(err =>
        Promise.reject(dispatch(actions.verifyForgotPasswordFailed(err)))
      )
  }
