import { useForm } from "react-hook-form"
import { connect, ConnectedProps } from "react-redux"
import { useHistory, Link } from "react-router-dom"
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import PublicLayout from "../../layouts/PublicLayout/PublicLayout"
import Input from "../../components/Input/Input"
import Button from "../../components/Button/Button"
import SEO from "../../components/SEO/SEO"
import { ReactComponent as EmailIcon } from "../../assets/images/email.svg"
import { ReactComponent as PasswordIcon } from "../../assets/images/password.svg"
import { ReactComponent as PersonIcon } from "../../assets/images/person.svg"
import { PATH } from "../../constants"
import { doRegister } from "./Register.thunks"
import * as actions from "../Register/Register.actions"
import * as actionValidations from "../../constants/validation"
import "./Register.scss"

const mapStateToProps = (state: AppState) => ({
  loading: state.register.loading,
  firstName: state.register.firstName,
  lastName: state.register.lastName,
  email: state.register.email,
  password: state.register.password,
  passwordConfirm: state.register.passwordConfirm,
  errorMessage: state.register.errorMessage
})

const mapDispatchToProps = dispatch => ({
  doRegister: (params: RequestRegister) => dispatch(doRegister(params)),
  setFirstName: (firstName: string) =>
    dispatch(actions.setFirstName(firstName)),
  setLastName: (lastName: string) => dispatch(actions.setLastName(lastName)),
  setEmail: (email: string) => dispatch(actions.setEmail(email)),
  setPassword: (password: string) => dispatch(actions.setPassword(password)),
  setPasswordConfirm: (passwordConfirm: string) =>
    dispatch(actions.setPasswordConfirm(passwordConfirm)),
  setErrorMessage: (message: string) =>
    dispatch(actions.setErrorMessage(message))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

const Register = (props: Props) => {
  const {
    doRegister,
    setFirstName,
    setLastName,
    setEmail,
    setPassword,
    setPasswordConfirm,
    setErrorMessage,
    loading,
    firstName,
    lastName,
    email,
    password,
    passwordConfirm,
    errorMessage
  } = props

  const history = useHistory()

  const handleFirstName = event => {
    register("firstName").onChange(event)
    setFirstName(event.target.value)
  }

  const handleLastName = event => {
    register("lastName").onChange(event)
    setLastName(event.target.value)
  }

  const handleEmail = event => {
    register("email").onChange(event)
    setEmail(event.target.value)
  }

  const handlePassword = event => {
    register("password").onChange(event)
    setPassword(event.target.value)
  }

  const handlePasswordConfirm = event => {
    register("passwordConfirm").onChange(event)
    setPasswordConfirm(event.target.value)
  }

  const checkPassword = () => password === passwordConfirm

  const submit = () => {
    if (!loading && checkPassword()) {
      doRegister({
        firstName,
        lastName,
        email,
        password,
        passwordConfirm
      }).then(() => {
        history.push(PATH.HOME)
      })
    } else {
      setErrorMessage(actionValidations.WRONG_PASSWORD)
    }
  }

  const schema = yup.object().shape({
    firstName: yup.string().required(actionValidations.REQUIRE),
    lastName: yup.string().required(actionValidations.REQUIRE),
    email: yup
      .string()
      .email(actionValidations.INVALID_EMAIL)
      .required(actionValidations.REQUIRE),
    password: yup
      .string()
      .min(
        actionValidations.PASSWORD_MIN_LENGTH,
        actionValidations.MIN_PASSWORD
      )
      .required(actionValidations.REQUIRE),
    passwordConfirm: yup
      .string()
      .min(
        actionValidations.PASSWORD_MIN_LENGTH,
        actionValidations.MIN_PASSWORD
      )
      .required(actionValidations.REQUIRE)
  })

  const {
    register,
    formState: { errors }
  } = useForm<RequestRegister>({
    mode: "onChange",
    resolver: yupResolver(schema)
  })
  return (
    <div>
      <SEO title="Register" description="This is register page" />
      <PublicLayout
        title="Let’s Get Started"
        subTitle="Create an new account"
        classStyle="register"
        loading={loading}
      >
        <div className="form">
          <Input
            registerInput={register}
            type="text"
            name="firstName"
            placeholder="First Name"
            errorMessage={errorMessage}
            errors={errors.firstName}
            onChange={handleFirstName}
          >
            <PersonIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          <Input
            registerInput={register}
            type="text"
            name="lastName"
            placeholder="Last Name"
            errorMessage={errorMessage}
            errors={errors.lastName}
            onChange={handleLastName}
          >
            <PersonIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          <Input
            registerInput={register}
            type="text"
            name="email"
            placeholder="Your Email"
            errorMessage={errorMessage}
            errors={errors.email}
            onChange={handleEmail}
          >
            <EmailIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          <Input
            name="password"
            registerInput={register}
            type="password"
            placeholder="Password"
            errorMessage={errorMessage}
            errors={errors.password}
            onChange={handlePassword}
          >
            <PasswordIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          <Input
            name="passwordConfirm"
            registerInput={register}
            type="password"
            placeholder="Password Again"
            errorMessage={errorMessage}
            errors={errors.passwordConfirm}
            onChange={handlePasswordConfirm}
          >
            <PasswordIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          {errorMessage && <p className="error">{errorMessage}</p>}
          <Button
            className="px-3 my-2"
            type="button"
            value="Sign Up"
            onClick={submit}
          />
        </div>
        <footer className="text-center">
          <p>
            have a account?
            <span className="text__color">
              <Link to={PATH.LOGIN} className="text__color">
                Sign in
              </Link>
            </span>
          </p>
        </footer>
      </PublicLayout>
    </div>
  )
}

export default connector(Register)
