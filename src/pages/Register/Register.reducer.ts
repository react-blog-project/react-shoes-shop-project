import * as types from "./Register.constants"
import * as typeValidations from "../../constants/validation"
import produce from "immer"

interface registerStateInterface {
  loading: boolean
  firstName: string
  lastName: string
  email: string
  password: string
  passwordConfirm: string
  errorMessage: string
}
const initialState: registerStateInterface = {
  loading: false,
  firstName: "",
  lastName: "",
  email: "",
  password: "",
  passwordConfirm: "",
  errorMessage: ""
}

export const registerReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.CHANGE_FIRST_NAME:
        draft.errorMessage = ""
        draft.firstName = action.firstName
        break
      case types.CHANGE_LAST_NAME:
        draft.errorMessage = ""
        draft.lastName = action.lastName
        break
      case typeValidations.CHANGE_EMAIL:
        draft.errorMessage = ""
        draft.email = action.email
        break
      case typeValidations.CHANGE_PASSWORD:
        draft.errorMessage = ""
        draft.password = action.password
        break
      case typeValidations.CHANGE_PASSWORD_CONFIRM:
        draft.errorMessage = ""
        draft.passwordConfirm = action.passwordConfirm
        break
      case typeValidations.CHANGE_ERROR:
        draft.errorMessage = action.errorMessage
        break
      case types.REGISTER_REQUESTED:
        draft.loading = true
        break
      case types.REGISTER_SUCCESS:
        draft.errorMessage = ""
        draft.loading = false
        break
      case types.REGISTER_FAILED:
        draft.errorMessage = action.errorMessage
        draft.loading = false
        break
      default:
        return state
    }
  })
