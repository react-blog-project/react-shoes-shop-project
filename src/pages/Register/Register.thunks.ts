import { postRegister } from "../../apis/user.api"
import * as actions from "./Register.actions"

export const doRegister = (payload: RequestRegister) => dispatch => {
  dispatch(actions.registerRequested())
  return postRegister(payload)
    .then(() => dispatch(actions.registerSuccess()))
    .catch(err => Promise.reject(dispatch(actions.registerFailed(err.message))))
}
