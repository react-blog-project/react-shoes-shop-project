import * as types from "./Register.constants"
import * as typeValidations from "../../constants/validation"

export const setFirstName = (firstName: string) => ({
  type: types.CHANGE_FIRST_NAME,
  firstName
})

export const setLastName = (lastName: string) => ({
  type: types.CHANGE_LAST_NAME,
  lastName
})

export const setEmail = (email: string) => ({
  type: typeValidations.CHANGE_EMAIL,
  email
})

export const setPassword = (password: string) => ({
  type: typeValidations.CHANGE_PASSWORD,
  password
})

export const setPasswordConfirm = (passwordConfirm: string) => ({
  type: typeValidations.CHANGE_PASSWORD_CONFIRM,
  passwordConfirm
})

export const setErrorMessage = (errorMessage: string) => ({
  type: typeValidations.CHANGE_ERROR,
  errorMessage
})

export const registerRequested = () => ({
  type: types.REGISTER_REQUESTED
})

export const registerSuccess = () => ({
  type: types.REGISTER_SUCCESS
})

export const registerFailed = (message: string) => ({
  type: types.REGISTER_FAILED,
  message
})
