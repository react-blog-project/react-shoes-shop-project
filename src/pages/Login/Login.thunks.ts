import { postLogin } from "../../apis/user.api"
import { getUserInfo } from "../../App/App.thunks"
import * as actionLogin from "./Login.actions"

export const doLogin = (payload: RequestLogin) => dispatch => {
  dispatch(actionLogin.loginRequested())
  return postLogin(payload)
    .then(() => {
      dispatch(actionLogin.loginSuccess())
      dispatch(getUserInfo())
    })
    .catch(err =>
      Promise.reject(dispatch(actionLogin.loginFailed(err.message)))
    )
}
