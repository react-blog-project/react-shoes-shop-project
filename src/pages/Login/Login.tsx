import { useForm } from "react-hook-form"
import { connect, ConnectedProps } from "react-redux"
import { useHistory, Link } from "react-router-dom"
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import PublicLayout from "../../layouts/PublicLayout/PublicLayout"
import Authentication from "../../components/Authentication/Authentication"
import Input from "../../components/Input/Input"
import Button from "../../components/Button/Button"
import SEO from "../../components/SEO/SEO"
import { ReactComponent as Facebook } from "../../assets/images/facebook.svg"
import { ReactComponent as Google } from "../../assets/images/google.svg"
import { ReactComponent as EmailIcon } from "../../assets/images/email.svg"
import { ReactComponent as PasswordIcon } from "../../assets/images/password.svg"
import { PATH } from "../../constants"
import { doLogin } from "./Login.thunks"
import * as actionValidations from "../../constants/validation"
import * as actions from "./Login.actions"
import "./Login.scss"

const mapStateToProps = (state: AppState) => ({
  loading: state.login.loading,
  email: state.login.email,
  password: state.login.password,
  errorMessage: state.login.errorMessage
})

const mapDispatchToProps = dispatch => ({
  doLogin: (params: RequestLogin) => dispatch(doLogin(params)),
  setEmail: (email: string) => dispatch(actions.setEmail(email)),
  setPassword: (password: string) => dispatch(actions.setPassword(password)),
  setErrorMessage: (m: string) => dispatch(actions.setErrorMessage(m))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function Login(props: Props) {
  const {
    doLogin,
    setEmail,
    setPassword,
    loading,
    email,
    password,
    errorMessage,
    setErrorMessage
  } = props
  const history = useHistory()

  const handleEmail = event => {
    register("email").onChange(event)
    setEmail(event.target.value)
    if (email && password) {
      setErrorMessage("")
    }
  }

  const handlePassword = event => {
    register("password").onChange(event)
    setPassword(event.target.value)
    if (email && password) {
      setErrorMessage("")
    }
  }

  const submit = (e: React.FormEvent) => {
    e.preventDefault()
    if (!loading) {
      doLogin({ email, password }).then(() => {
        history.push(PATH.HOME)
      })
    }
  }

  const schema = yup.object().shape({
    email: yup
      .string()
      .email(actionValidations.INVALID_EMAIL)
      .required(actionValidations.REQUIRE),
    password: yup
      .string()
      .min(
        actionValidations.PASSWORD_MIN_LENGTH,
        actionValidations.MIN_PASSWORD
      )
      .required(actionValidations.REQUIRE)
  })
  const {
    register,
    formState: { errors }
  } = useForm<RequestLogin>({
    mode: "onChange",
    resolver: yupResolver(schema)
  })

  return (
    <div>
      <SEO title="Login" description="This is login page" />
      <PublicLayout
        title="Hello"
        subTitle="Sign in to continue"
        classStyle="login"
        loading={loading}
      >
        <form className="form" method="post" onSubmit={submit}>
          <Input
            registerInput={register}
            type="text"
            name="email"
            placeholder="Your Email"
            errorMessage={errorMessage}
            errors={errors.email}
            onChange={handleEmail}
          >
            <EmailIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          <Input
            name="password"
            registerInput={register}
            type="password"
            placeholder="Password"
            errorMessage={errorMessage}
            errors={errors.password}
            onChange={handlePassword}
          >
            <PasswordIcon className={errorMessage ? "err__icon" : ""} />
          </Input>
          {errorMessage && <p className="error">{errorMessage}</p>}
          <Button
            className="px-3 my-2"
            type="submit"
            value="Sign in"
            disabled={!!errorMessage}
          />

          <div className="formDivSpan text-center my-4">
            <span className="formSpan">OR</span>
          </div>

          <Authentication title="Login with Google">
            <Google className="login__icon" />
          </Authentication>
          <Authentication title="Login with Facebook">
            <Facebook className="login__icon" />
          </Authentication>
        </form>

        <footer className="text-center">
          <p className="text__color">
            <Link to={PATH.FORGOT_PASSWORD}>Forgot Password?</Link>
          </p>
          <p>
            Don’t have a account?
            <span className="text__color">
              <Link to={PATH.REGISTER}> Register</Link>
            </span>
          </p>
        </footer>
      </PublicLayout>
    </div>
  )
}

export default connector(Login)
