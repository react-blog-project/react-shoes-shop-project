import * as types from "./Login.constants"
import * as typeValidations from "../../constants/validation"
import produce from "immer"

interface loginStateInterFace {
  loading: boolean
  email: string
  password: string
  errorMessage: string
}
const initialState: loginStateInterFace = {
  loading: false,
  email: "",
  password: "",
  errorMessage: ""
}

export const loginReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case typeValidations.CHANGE_EMAIL:
        draft.email = action.payload
        break
      case typeValidations.CHANGE_PASSWORD:
        draft.password = action.payload
        break
      case typeValidations.CHANGE_ERROR:
        draft.errorMessage = action.payload
        break
      case types.LOGIN_REQUEST:
        draft.loading = true
        break
      case types.LOGIN_SET_AUTHENTICATION:
        draft.loading = false
        draft.errorMessage = action.data.errorMessage
        draft.password = ""
        break
      default:
        return state
    }
  })
