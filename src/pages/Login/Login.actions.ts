import * as types from "./Login.constants"
import * as typeValidations from "../../constants/validation"

export const setEmail = payload => ({
  type: typeValidations.CHANGE_EMAIL,
  payload
})

export const setPassword = payload => ({
  type: typeValidations.CHANGE_PASSWORD,
  payload
})

export const setErrorMessage = payload => ({
  type: typeValidations.CHANGE_ERROR,
  payload
})

export const loginRequested = () => ({
  type: types.LOGIN_REQUEST
})

export const loginSuccess = () => ({
  type: types.LOGIN_SET_AUTHENTICATION,
  data: {
    isAuthenticated: true,
    errorMessage: null
  }
})

export const loginFailed = (errorMessage: string) => ({
  type: types.LOGIN_SET_AUTHENTICATION,
  data: {
    isAuthenticated: false,
    errorMessage
  }
})
