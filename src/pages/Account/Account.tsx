import { Link, useHistory } from "react-router-dom"
import { connect, ConnectedProps } from "react-redux"
import { ReactComponent as Address } from "../../assets/images/Account/address.svg"
import { ReactComponent as Order } from "../../assets/images/Account/order.svg"
import { ReactComponent as Payment } from "../../assets/images/Account/payment.svg"
import { ReactComponent as Profile } from "../../assets/images/profile.svg"
import { ReactComponent as Logout } from "../../assets/images/Account/phone.svg"
import BottomNav from "../../components/Navigation/MobileNavigation/BottomNav"
import TitleHeader from "../../components/TitleHeader/TitleHeader"
import SEO from "../../components/SEO/SEO"
import { PATH } from "../../constants"
import { doLogOut } from "./Account.thunk"
import "./Account.scss"

function AccountItem({ path, children, content }) {
  return (
    <Link to={`${path}`} className="d-flex text-decoration-none account-item">
      {children}
      <span>{content}</span>
    </Link>
  )
}
const mapStateToProps = (state: AppState) => ({})

const mapDispatchToProps = dispatch => ({
  doLogOut: () => dispatch(doLogOut())
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function Account(props: Props) {
  const { doLogOut } = props
  const history = useHistory()

  const handleLogout = () => {
    doLogOut().then(() => history.push(PATH.LOGIN))
  }

  return (
    <div className="d-flex justify-content-center">
      <SEO title="Account" description="This is Account page" />
      <div className="account-content shadow">
        <TitleHeader content="Account" />
        <AccountItem path={PATH.PROFILE} content="Profile">
          <Profile className="profile-icon" />
        </AccountItem>
        <AccountItem path={PATH.ORDER} content="Order">
          <Order className="order-icon" />
        </AccountItem>
        <AccountItem path={PATH.ADDRESS} content="Address">
          <Address className="address-icon" />
        </AccountItem>
        <AccountItem path={PATH.PAYMENT} content="Payment">
          <Payment className="payment-icon" />
        </AccountItem>
        <div
          className="d-flex text-decoration-none account-item"
          onClick={handleLogout}
        >
          <Logout className="logout-icon" />
          <span>Log out</span>
        </div>
        <BottomNav />
      </div>
    </div>
  )
}

export default connector(Account)
