import { postLogout } from "../../apis/user.api"
import * as actions from "./Account.actions"

export const doLogOut = () => dispatch => {
  dispatch(actions.logOutRequested())
  return postLogout()
    .then(() => dispatch(actions.logOutSuccess()))
    .catch(err => Promise.reject(dispatch(actions.logOutFailed(err))))
}
