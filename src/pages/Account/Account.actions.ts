import * as types from "./Account.constants"

export const logOutRequested = () => ({
  type: types.LOG_OUT_REQUESTED
})

export const logOutSuccess = () => ({
  type: types.LOG_OUT_SUCCESS
})

export const logOutFailed = (message: string) => ({
  type: types.LOG_OUT_FAILED,
  message
})
