import * as types from "./Account.constants"
import produce from "immer"

interface accountInterface {
  loading: boolean
  errorMessage: string
}

const initialState: accountInterface = {
  loading: false,
  errorMessage: ""
}

export const accountReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.LOG_OUT_REQUESTED:
        draft.loading = true
        break
      case types.LOG_OUT_SUCCESS:
        draft.loading = false
        break
      case types.LOG_OUT_FAILED:
        draft.errorMessage = action.errorMessage
        draft.loading = false
        break
      default:
        return state
    }
  })
