import * as types from "./ChangeProfile.constants"
import * as typeValidations from "../../constants/validation"

export const setBirthday = (birthday: string) => ({
  type: types.CHANGE_BIRTHDAY,
  birthday
})

export const setEmail = (email: string) => ({
  type: typeValidations.CHANGE_EMAIL,
  email
})

export const setFirstName = (firstName: string) => ({
  type: types.CHANGE_FIRST_NAME,
  firstName
})

export const setGender = (gender: number) => ({
  type: types.CHANGE_GENDER,
  gender
})

export const setLastName = (lastName: string) => ({
  type: types.CHANGE_LAST_NAME,
  lastName
})

export const setMobile = (mobile: string) => ({
  type: types.CHANGE_MOBILE,
  mobile
})

export const setErrorMessage = (errorMessage: string) => ({
  type: typeValidations.CHANGE_ERROR,
  errorMessage
})

export const changeProfileRequested = () => ({
  type: types.CHANGE_PROFILE_REQUEST
})

export const changeProfileSuccess = (profile: any) => ({
  type: types.CHANGE_PROFILE_SUCCESS,
  profile
})

export const changeProfileFailed = (errorMessage: string) => ({
  type: types.CHANGE_PROFILE_FAILED,
  errorMessage
})
