import { yupResolver } from "@hookform/resolvers/yup"
import { useForm } from "react-hook-form"
import { connect, ConnectedProps } from "react-redux"
import { useHistory } from "react-router-dom"
import * as yup from "yup"
import { ReactComponent as PhoneIcon } from "../../assets/images/Account/phone.svg"
import { ReactComponent as DateIcon } from "../../assets/images/date.svg"
import { ReactComponent as EmailIcon } from "../../assets/images/email.svg"
import Button from "../../components/Button/Button"
import Dropdown from "../../components/Dropdown/Dropdown"
import Input from "../../components/Input/Input"
import SEO from "../../components/SEO/SEO"
import { PATH } from "../../constants"
import * as actionValidations from "../../constants/validation"
import { formatDate, formatGender } from "../../hooks/useFormatData"
import { GENDER } from "../../constants/styles"
import PublicLayout from "../../layouts/PublicLayout/PublicLayout"
import * as actions from "./ChangeProfile.actions"
import "./ChangeProfile.scss"
import { doChangeProfile } from "./ChangeProfile.thunks"

const mapStateToProps = (state: AppState) => ({
  loading: state.changeProfile.loading,
  user: state.app.user,
  profile: state.changeProfile.profile,
  errorMessage: state.changeProfile.errorMessage
})
const mapDispatchToProps = dispatch => ({
  setBirthday: (birthday: string) => dispatch(actions.setBirthday(birthday)),
  setEmail: (email: string) => dispatch(actions.setEmail(email)),
  setFirstName: (firstName: string) =>
    dispatch(actions.setFirstName(firstName)),

  setGender: (gender: number) => dispatch(actions.setGender(gender)),
  setLastName: (lastName: string) => dispatch(actions.setLastName(lastName)),
  setMobile: (mobile: string) => dispatch(actions.setMobile(mobile)),
  doChangeProfile: (profile: any) => dispatch(doChangeProfile(profile))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function ChangeProfile(props: Props) {
  const {
    loading,
    user,
    profile,
    errorMessage,
    setBirthday,
    setEmail,
    setFirstName,
    setGender,
    setLastName,
    setMobile,
    doChangeProfile
  } = props
  const { birthday, firstName, gender, lastName, mobile } = profile
  const history = useHistory()

  const handleFirstName = event => {
    register("firstName").onChange(event)
    setFirstName(event.target.value)
  }
  const handleLastName = event => {
    register("lastName").onChange(event)
    setLastName(event.target.value)
  }
  const handleBirthDay = event => {
    register("birthday").onChange(event)
    setBirthday(event.target.value)
  }

  const handleEmail = event => {
    register("email").onChange(event)
    setEmail(event.target.value)
  }
  const handleMobile = event => {
    register("mobile").onChange(event)
    setMobile(event.target.value)
  }
  const handleGender = gender => {
    gender === GENDER.MALE ? setGender(1) : setGender(2)
  }

  const schema = yup.object().shape({
    email: yup.string().email(actionValidations.INVALID_EMAIL)
  })
  const {
    register,
    setValue,
    formState: { errors }
  } = useForm<UserInterface>({
    mode: "onChange",
    resolver: yupResolver(schema)
  })
  const setValueProfile = () => {
    setValue("birthday", formatDate(user.birthday))
    setValue("email", user.email)
    setValue("firstName", user.firstName)
    setValue("gender", user.gender)
    setValue("lastName", user.lastName)
    setValue("mobile", user.mobile)
  }
  setValueProfile()

  const submit = (e: React.FormEvent) => {
    e.preventDefault()
    if (!loading) {
      doChangeProfile({
        birthday,
        firstName,
        gender,
        lastName,
        mobile
      }).then(() => {
        history.push(PATH.PROFILE)
      })
    }
  }

  return (
    <div>
      <SEO title="Change Profile" description="This is change profile page" />
      <PublicLayout
        title="Change Profile"
        subTitle="Change your profile "
        classStyle="change-profile"
        loading={loading}
      >
        <form className="form" method="patch" onSubmit={submit}>
          <Input
            name="firstName"
            registerInput={register}
            type="text"
            placeholder="First Name"
            className="input-profile"
            label="First Name"
            onChange={handleFirstName}
          />
          <Input
            name="lastName"
            registerInput={register}
            type="text"
            placeholder="Last Name"
            className="input-profile"
            label="Last Name"
            onChange={handleLastName}
          />
          <Dropdown
            option={[GENDER.MALE, GENDER.FEMALE]}
            onChange={handleGender}
            label="Choose Gender"
            value={gender !== 3 ? formatGender(gender) : "Select Gender"}
          />

          <div className="date-profile">
            <Input
              name="birthday"
              registerInput={register}
              type="date"
              placeholder="Your Birthday"
              className="input-profile date"
              label="Your Birthday"
              onChange={handleBirthDay}
            />
            <DateIcon className="date-icon" />
          </div>

          <Input
            disabled={true}
            label="Email"
            registerInput={register}
            type="text"
            name="email"
            className="email-profile"
            placeholder="Your Email"
            errors={errors.email}
            onChange={handleEmail}
          >
            <EmailIcon className="email-icon" />
          </Input>

          <Input
            label="Phone Number"
            registerInput={register}
            type="text"
            name="mobile"
            className="phone-profile"
            placeholder="Phone Number"
            onChange={handleMobile}
          >
            <PhoneIcon className="phone-icon" />
          </Input>
          {errorMessage && <p className="error">{errorMessage}</p>}
          <Button type="submit" className="my-4" value="Change profile" />
        </form>
      </PublicLayout>
    </div>
  )
}

export default connector(ChangeProfile)
