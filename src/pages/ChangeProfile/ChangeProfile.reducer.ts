import produce from "immer"
import * as typeValidations from "../../constants/validation"
import * as typeApps from "../../App/App.constants"
import * as types from "./ChangeProfile.constants"

interface loginStateInterFace {
  loading: boolean
  profile: UserInterface
  errorMessage: string
}
const initialState: loginStateInterFace = {
  loading: false,
  profile: {
    id: 0,
    email: "",
    mobile: "",
    firstName: "",
    lastName: "",
    gender: 3,
    birthday: "",
    status: "",
    createdAt: null,
    updatedAt: null,
    address: "",
    role: "",
    isTwoFactorAuthenticationEnabled: false
  },
  errorMessage: ""
}

export const changeProfileReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case typeApps.USER_REQUEST_INFO_SUCCESS:
        draft.profile = action.user as UserInterface
        draft.loading = false
        break
      case types.CHANGE_BIRTHDAY:
        draft.profile.birthday = action.birthday
        break
      case typeValidations.CHANGE_EMAIL:
        draft.profile.email = action.email
        break
      case types.CHANGE_FIRST_NAME:
        draft.profile.firstName = action.firstName
        break
      case types.CHANGE_GENDER:
        draft.profile.gender = action.gender
        break
      case types.CHANGE_LAST_NAME:
        draft.profile.lastName = action.lastName
        break
      case types.CHANGE_MOBILE:
        draft.profile.mobile = action.mobile
        break
      case types.CHANGE_PROFILE_REQUEST:
        draft.loading = true
        break
      case types.CHANGE_PROFILE_SUCCESS:
        draft.loading = false
        break
      case types.CHANGE_PROFILE_FAILED:
        draft.errorMessage = action.errorMessage
        draft.loading = false
        break
      default:
        return state
    }
  })
