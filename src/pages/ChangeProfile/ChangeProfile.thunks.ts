import { patchChangeProfile } from "../../apis/profile.api"
import * as actions from "./ChangeProfile.actions"

export const doChangeProfile = (payload: any) => dispatch => {
  dispatch(actions.changeProfileRequested())
  return patchChangeProfile(payload)
    .then(() => {
      dispatch(actions.changeProfileSuccess(payload))
    })
    .catch(err =>
      Promise.reject(dispatch(actions.changeProfileFailed(err.message)))
    )
}
