export const FORGOT_PASSWORD_REQUESTED = "FORGOT_PASSWORD_REQUESTED"
export const FORGOT_PASSWORD_SUCCESS = "FORGOT_PASSWORD_SUCCESS"
export const FORGOT_PASSWORD_FAILED = "FORGOT_PASSWORD_FAILED"
