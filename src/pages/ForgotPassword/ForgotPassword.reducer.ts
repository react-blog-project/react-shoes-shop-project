import * as types from "./ForgotPassword.constants"
import * as typeValidations from "../../constants/validation"
import produce from "immer"

interface forgotPasswordStateInterFace {
  loading: boolean
  isSendEmail: boolean
  email: string
  errorMessage: string
}

const initialState: forgotPasswordStateInterFace = {
  loading: false,
  isSendEmail: false,
  email: "",
  errorMessage: ""
}

export const forgotPasswordReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case typeValidations.CHANGE_EMAIL:
        draft.email = action.email
        draft.errorMessage = ""
        break

      case types.FORGOT_PASSWORD_REQUESTED:
        draft.loading = true
        draft.isSendEmail = false
        break
      case types.FORGOT_PASSWORD_SUCCESS:
        draft.isSendEmail = true
        draft.loading = false
        draft.errorMessage = ""
        break
      case types.FORGOT_PASSWORD_FAILED:
        draft.loading = false
        draft.errorMessage = action.errorMessage
        break
      default:
        return state
    }
  })
