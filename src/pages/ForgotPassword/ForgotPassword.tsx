import { yupResolver } from "@hookform/resolvers/yup"
import { FormEvent } from "react"
import { useForm } from "react-hook-form"
import { connect, ConnectedProps } from "react-redux"
import { useHistory } from "react-router-dom"
import * as yup from "yup"
import { ReactComponent as EmailIcon } from "../../assets/images/email.svg"
import Button from "../../components/Button/Button"
import Input from "../../components/Input/Input"
import PublicLayout from "../../layouts/PublicLayout/PublicLayout"
import SEO from "../../components/SEO/SEO"
import { PATH } from "../../constants"
import { doForgotPassword } from "./ForgotPassword.thunks"
import * as actionValidations from "../../constants/validation"
import * as actions from "./ForgotPassword.actions"
import "./ForgotPassword.scss"

const mapStateToProps = (state: AppState) => ({
  loading: state.forgotPassword.loading,
  isSendEmail: state.forgotPassword.isSendEmail,
  email: state.forgotPassword.email,
  errorMessage: state.forgotPassword.errorMessage
})

const mapDispatchToProps = dispatch => ({
  doForgotPassword: (params: RequestResetPassword) =>
    dispatch(doForgotPassword(params)),
  setEmail: (email: string) => dispatch(actions.setEmail(email))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function ForgotPassword(props: Props) {
  const {
    doForgotPassword,
    setEmail,
    loading,
    email,
    isSendEmail,
    errorMessage
  } = props
  const history = useHistory()

  const handleEmail = event => {
    register("email").onChange(event)
    setEmail(event.target.value)
  }

  const submit = (e: FormEvent) => {
    e.preventDefault()
    if (!loading && !isSendEmail) doForgotPassword({ email })
    else history.push(PATH.LOGIN)
  }

  const schema = yup.object().shape({
    email: yup
      .string()
      .email(actionValidations.INVALID_EMAIL)
      .required(actionValidations.REQUIRE)
  })
  const {
    register,
    formState: { errors }
  } = useForm<RequestResetPassword>({
    mode: "onChange",
    resolver: yupResolver(schema)
  })
  const emailSent = (
    <div className="emailSent text-center">
      <p>
        Mã xác minh đã được gửi đến email <span>{email}</span>. Vui lòng xác
        minh.
      </p>
    </div>
  )
  const renderSendEmail = isSendEmail ? (
    emailSent
  ) : (
    <>
      <Input
        registerInput={register}
        type="text"
        name="email"
        placeholder="Your Email"
        errorMessage={errorMessage}
        errors={errors.email}
        onChange={handleEmail}
      >
        <EmailIcon className={errorMessage ? "err__icon" : ""} />
      </Input>
      {errorMessage && <p className="error">{errorMessage}</p>}
    </>
  )

  return (
    <div>
      <SEO title="Forgot password" description="This is forgot password page" />
      <PublicLayout
        title="Welcome to E-com"
        subTitle="Reset your password"
        classStyle="forgot__password"
        loading={loading}
      >
        <form className="form" method="post" onSubmit={submit}>
          {renderSendEmail}
          <Button
            className="px-3 my-2"
            type="submit"
            value={isSendEmail ? "Next" : "Send password reset email"}
            disabled={!!errorMessage}
          />
        </form>
      </PublicLayout>
    </div>
  )
}

export default connector(ForgotPassword)
