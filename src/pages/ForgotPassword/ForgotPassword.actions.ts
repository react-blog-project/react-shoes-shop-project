import * as types from "./ForgotPassword.constants"
import * as typeValidations from "../../constants/validation"

export const setEmail = (email: string) => ({
  type: typeValidations.CHANGE_EMAIL,
  email
})

export const forgotPasswordRequested = () => ({
  type: types.FORGOT_PASSWORD_REQUESTED
})

export const forgotPasswordSuccess = () => ({
  type: types.FORGOT_PASSWORD_SUCCESS
})

export const forgotPasswordFailed = (errorMessage: string) => ({
  type: types.FORGOT_PASSWORD_FAILED,
  errorMessage
})
