import { postForgotPassword } from "../../apis/user.api"
import * as actions from "./ForgotPassword.actions"

export const doForgotPassword = (payload: RequestResetPassword) => dispatch => {
  dispatch(actions.forgotPasswordRequested())

  return postForgotPassword(payload)
    .then(() => dispatch(actions.forgotPasswordSuccess()))
    .catch(err =>
      Promise.reject(dispatch(actions.forgotPasswordFailed(err.message)))
    )
}
