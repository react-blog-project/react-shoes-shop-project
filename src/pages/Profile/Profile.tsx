import React from "react"
import { connect, ConnectedProps } from "react-redux"
import { Link } from "react-router-dom"
import { ReactComponent as IconBirthday } from "../../assets/images/Account/birthday.svg"
import { ReactComponent as IconGender } from "../../assets/images/Account/gender.svg"
import { ReactComponent as IconPhone } from "../../assets/images/Account/phone.svg"
import { ReactComponent as Avatar } from "../../assets/images/avatar.svg"
import { ReactComponent as IconEmail } from "../../assets/images/email.svg"
import { ReactComponent as IconPassword } from "../../assets/images/password.svg"
import Button from "../../components/Button/Button"
import { formatDate, formatGender } from "../../hooks/useFormatData"
import AccountLayout from "../../layouts/AccountLayout/AccountLayout"
import "./Profile.scss"

const mapStateToProps = (state: AppState) => ({
  user: state.app.user
})

const mapDispatchToProps = {}

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function ProfileItem(props) {
  return (
    <div className="d-flex profile-item">
      {props.children}
      <span className="profile-item__title">{props.title}</span>
      <span className="profile-item__content">{props.content}</span>
    </div>
  )
}

function Profile(props: Props) {
  const { user } = props

  return (
    <AccountLayout title="Profile" pathBack="account">
      <div className="profile__header d-flex">
        <div className="profile__header--left">
          <Avatar />
        </div>
        <div className="profile__header--right">
          <p className="name">
            {user.firstName} {user.lastName}
          </p>
          <p className="username">{user.email}</p>
        </div>
      </div>
      <ProfileItem
        path="profile"
        title="Gender"
        content={user.gender !== 3 ? formatGender(user.gender) : "Not selected"}
      >
        <IconGender />
      </ProfileItem>
      <ProfileItem
        path="profile"
        title="Birthday"
        content={formatDate(user.birthday)}
      >
        <IconBirthday />
      </ProfileItem>
      <ProfileItem path="profile" title="Email" content={user.email}>
        <IconEmail className="email" />
      </ProfileItem>
      <ProfileItem path="profile" title="Phone Number" content={user.mobile}>
        <IconPhone />
      </ProfileItem>
      <ProfileItem
        path="profile"
        title="Change Password"
        content="•••••••••••••••••"
      >
        <IconPassword className="password" />
      </ProfileItem>

      <div className="profile__button">
        <Link to="/change-profile">
          <Button type="button" className="my-4" value="Change profile" />
        </Link>
        <Link to="/change-password">
          <Button type="button" value="Change password" />
        </Link>
      </div>
    </AccountLayout>
  )
}

export default connector(Profile)
