import { useState } from "react"
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Thumbs, Pagination } from "swiper"
import "./CarouselProduct.scss"

SwiperCore.use([Thumbs, Pagination])

interface Props {
  images: {
    name: string | null
    url: string
    width: null
    height: null
    description: null
  }[]
}

function CarouselProduct(props: Props) {
  const { images } = props

  const [thumbsSwiper, setThumbsSwiper] = useState<SwiperCore | null>(null)

  return (
    <div className="swiper d-flex flex-column">
      <Swiper
        className="carousel"
        spaceBetween={1}
        slidesPerView={1}
        thumbs={{ swiper: thumbsSwiper }}
        pagination={{
          clickable: true
        }}
      >
        {images?.map((image, index) => (
          <SwiperSlide key={index}>
            <img src={image.url} alt="product" />
          </SwiperSlide>
        ))}
      </Swiper>
      <Swiper
        className="thumb"
        onSwiper={setThumbsSwiper}
        spaceBetween={10}
        slidesPerView={4}
      >
        {images?.map((image, index) => (
          <SwiperSlide key={index}>
            <img src={image.url} alt="product" />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  )
}

export default CarouselProduct
