import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
import "swiper/swiper.min.css"
import "swiper/components/pagination/pagination.min.css"
import { ReactComponent as RatingIcon } from "../../../../assets/images/rating.svg"
import BEST_SELLER_PRODUCT from "../../../../assets/images/product5.png"
import "./BestSeller.scss"

SwiperCore.use([Pagination])

export default function App() {
  const items = [1, 2, 3, 4]
  return (
    <div className="best--seller">
      <h6>BEST SELLER</h6>
      <Swiper
        spaceBetween={20}
        slidesPerView={1}
        pagination={{
          clickable: true
        }}
        className="swiper__best--seller"
      >
        {items.map(item => (
          <SwiperSlide key={item} className="d-flex flex-column">
            <img src={BEST_SELLER_PRODUCT} alt="product" />
            <div className="rating">
              <RatingIcon className="rating-yellow" />
              <RatingIcon className="rating-yellow" />
              <RatingIcon className="rating-yellow" />
              <RatingIcon className="rating-yellow" />
              <RatingIcon className="rating-yellow" />
            </div>
            <div className="price d-flex">
              <p className="mr-3">$499</p>
              <p>$599</p>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  )
}
