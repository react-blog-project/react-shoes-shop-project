import { getProductBySlugApi } from "../../../apis/product.api"
import * as action from "./ProductDetails.actions"

export const getProductBySlug = (slug: string) => dispatch => {
  dispatch(action.getProductBySlugRequested)
  return getProductBySlugApi(slug)
    .then(res => {
      dispatch(action.getProductBySlugSuccess(res.data))
    })
    .catch(err =>
      Promise.reject(dispatch(action.getProductBySlugFailed(err.message)))
    )
}
