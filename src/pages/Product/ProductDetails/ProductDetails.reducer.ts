import * as types from "./ProductDetails.constant"
import produce from "immer"

interface productDetailsInterFace {
  loading: boolean
  error: string
  product: ProductDetails | null
}

const initialState: productDetailsInterFace = {
  loading: false,
  error: "",
  product: null
}

export const productDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.GET_PRODUCT_BY_SLUG_REQUEST:
        draft.loading = true
        break
      case types.GET_PRODUCT_BY_SLUG_SUCCESS:
        draft.loading = false
        draft.product = action.product
        break
      case types.GET_PRODUCT_BY_SLUG_FAIL:
        draft.loading = false
        draft.error = action.error
        break
      default:
        return state
    }
  })
