import * as types from "./ProductDetails.constant"

export const getProductBySlugRequested = () => ({
  type: types.GET_PRODUCT_BY_SLUG_REQUEST
})

export const getProductBySlugSuccess = (product: ProductDetails) => ({
  type: types.GET_PRODUCT_BY_SLUG_SUCCESS,
  product: product
})

export const getProductBySlugFailed = (errorMessage: string) => ({
  type: types.GET_PRODUCT_BY_SLUG_FAIL,
  error: errorMessage
})
