import ProductItem from "../../../../components/ProductItem/ProductItem"
import "./RelatedProduct.scss"

type Props = {
  listProducts: Product[]
}

export default function RelatedProduct(props: Props) {
  const { listProducts } = props

  return (
    <div className="related-product">
      <h3 className="title text-center pb-3 pt-2">RELATED PRODUCTS</h3>
      <div className="products">
        {listProducts.map((product, index) =>
          index < 4 ? (
            <div className="col-6 col-md-3 " key={index}>
              <ProductItem product={product} />
            </div>
          ) : null
        )}
      </div>
    </div>
  )
}
