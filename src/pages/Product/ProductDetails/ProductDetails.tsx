import { useState, useEffect } from "react"
import { connect, ConnectedProps } from "react-redux"
import { useHistory, useParams } from "react-router-dom"
import BestSeller from "./BestSeller/BestSeller"
import RelatedProduct from "./RelatedProduct/RelatedProduct"
import MainLayout from "../../../layouts/MainLayout/MainLayout"
import Button from "../../../components/Button/Button"
import CarouselProduct from "./CarouselProduct/CarouselProduct"
import Information from "./Information/Information"
import PageLoader from "src/components/Loading/PageLoader"
import { BREAKPOINT } from "../../../constants/styles"
import { ReactComponent as RatingIcon } from "../../../assets/images/rating.svg"
import { ReactComponent as CartIcon } from "../../../assets/images/cart_icon.svg"
import { ReactComponent as HeartIcon } from "../../../assets/images/hearts.svg"
import { ReactComponent as FacebookIcon } from "../../../assets/images/facebook.svg"
import { ReactComponent as TwitterIcon } from "../../../assets/images/twitter.svg"
import { useWindowSize } from "src/hooks/useWindowSize"
import { getProductList } from "src/pages/Home/Home.thunk"
import { getProductBySlug } from "./ProductDetails.thunks"
import "./ProductDetails.scss"

const mapStateToProps = (state: AppState) => ({
  loading: state.productDetails.loading,
  error: state.productDetails.error,
  product: state.productDetails.product,
  productId: state.home.productId,
  listProducts: state.home.listProducts
})

const mapDispatchToProps = dispatch => ({
  getProductBySlug: (slug: string) => dispatch(getProductBySlug(slug)),
  getProductList: (page: number) => dispatch(getProductList(page))
})

const connector = connect(mapStateToProps, mapDispatchToProps)

interface Props extends ConnectedProps<typeof connector> {}

function ProductDetails(props: Props) {
  const { loading, product, listProducts, getProductBySlug, getProductList } =
    props
  let { slug }: any = useParams()

  const [width] = useWindowSize()
  const history = useHistory()

  const widthTablet = width <= BREAKPOINT.MD ? true : false

  const items = [1, 2, 3, 4, 5]
  const [color, setColor] = useState("blue")
  const [quantityProduct, setQuantityProduct] = useState(1)

  const handleSelectColor = (color: string) => {
    setColor(color)
  }

  const handleDec = () => {
    if (quantityProduct > 1) setQuantityProduct(quantityProduct - 1)
  }

  const handleInc = () => {
    setQuantityProduct(quantityProduct + 1)
  }

  useEffect(() => {
    getProductBySlug(slug)
  }, [getProductBySlug, slug])

  useEffect(() => {
    if (!listProducts.length) getProductList(0)
  }, [getProductList, listProducts.length])

  const handleBack = () => {
    history.goBack()
  }

  return (
    <MainLayout>
      <div className="breadcrumbs d-flex justify-content-sm-center align-items-center">
        <div className="icon-back text-sm-start" onClick={handleBack}></div>
        <span className="text">Home</span>
        <i>/</i>
        <span className="text">Hot Deal</span>
        <i>/</i>
        <span className="product-name">{product?.name}</span>
      </div>
      <div className="product d-flex">
        <div className="product__detail">
          <div className="d-sm-flex">
            <CarouselProduct images={product?.images} />
            <div className="property">
              <div className="group">
                <h5 className="product__name">{product?.name}</h5>
                {items.map((item, index) => (
                  <RatingIcon
                    className={item <= product?.rate ? "rating-yellow" : ""}
                    key={index}
                  />
                ))}
                <span className="review">0 reviews</span>
                <span className="submit__review">Submit a view</span>
              </div>
              <div className="group mt-3">
                <span className="current-price pr-2">
                  ${product?.salePrice}
                </span>
                <span className="price pr-2">${product?.sellingPrice}</span>
                <span className="sale">24% Off</span>
                <div className="mt-3">
                  <span className="type">Availability:</span>
                  <span>In stock</span>
                </div>
                <div className="mt-3">
                  <span className="type">Category:</span>
                  <span>Accessories</span>
                </div>
                <p className="mt-3">Free shipping</p>
              </div>
              <div className="group mt-3">
                <div className="d-flex align-items-center">
                  <p className="type">Select Color:</p>
                  {["blue", "red", "black", "yellow"].map((item, index) => (
                    <div
                      key={index}
                      className={`color color--${item} mr-3`}
                      onClick={() => handleSelectColor(item)}
                    >
                      <div
                        className={color === item ? "select--color" : ""}
                        style={
                          color === item ? { border: `2px solid ${item}` } : {}
                        }
                      ></div>
                    </div>
                  ))}
                </div>
                <div className="d-flex align-items-center col-6 mt-3">
                  <p className="type">Size</p>
                  <select className="form-select" defaultValue="XS">
                    {["XS", "S", "M", "L"].map((item, index) => (
                      <option key={index} value={item}>
                        {item}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="group mt-3 d-flex justify-content-between">
                <div className="btn btn__number d-flex justify-content-between">
                  <span onClick={handleDec}>-</span>
                  <p>{quantityProduct}</p>
                  <span onClick={handleInc}>+</span>
                </div>
                <div className="d-flex">
                  <div className="btn btn__add-to-cart d-flex align-items-center">
                    <CartIcon />
                    <p className="d-none d-lg-inline ml-2">Add to cart</p>
                  </div>
                  <div className="btn btn__love ml-3">
                    <HeartIcon />
                  </div>
                </div>
              </div>
              <div className="d-inline d-sm-none group mt-3 d-flex justify-content-between">
                <Button type="button" value="Add to cart" />
              </div>
              <div className="d-flex justify-content-between mt-3">
                <div className="btn btn__facebook d-flex justify-content-center align-items-center">
                  <FacebookIcon className="icon icon__facebook" />
                  <p className="m-0 ml-2">
                    {widthTablet ? "" : "Share on "}Facebook
                  </p>
                </div>
                <div className="btn btn__twitter d-flex justify-content-center align-items-center">
                  <TwitterIcon className="icon" />
                  <p className="m-0 ml-2">
                    {widthTablet ? "" : "Share on "}Twitter
                  </p>
                </div>
              </div>
            </div>
          </div>
          <Information />
        </div>
        <div className="best-seller">
          <BestSeller />
        </div>
      </div>
      <div className="related-product-container">
        <RelatedProduct listProducts={listProducts} />
      </div>
      {loading && <PageLoader />}
    </MainLayout>
  )
}

export default connector(ProductDetails)
