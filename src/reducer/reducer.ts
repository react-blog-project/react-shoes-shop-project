import { combineReducers } from "redux"
import { AppReducer } from "../App/App.reducer"
import { loginReducer } from "../pages/Login/Login.reducer"
import { forgotPasswordReducer } from "../pages/ForgotPassword/ForgotPassword.reducer"
import { registerReducer } from "../pages/Register/Register.reducer"
import { VerifyForgotPasswordReducer } from "../pages/VerifyForgotPassword/VerifyForgotPassword.reducer"
import { changePasswordReducer } from "../pages/ChangePassword/ChangePassword.reducer"
import { productDetailsReducer } from "../pages/Product/ProductDetails/ProductDetails.reducer"
import { homeReducer } from "../pages/Home/Home.reducer"
import { cartReducer } from "./../pages/Cart/Cart.reducer"
import { addAddressReducer } from "./../pages/Address/AddAddress/AddAddress.reducer"
import { listAddressReducer } from "../pages/Address/AddressList/AddressList.reducer"
import { changeProfileReducer } from "./../pages/ChangeProfile/ChangeProfile.reducer"
import { accountReducer } from "../pages/Account/Account.reducer"

const rootReducer = combineReducers({
  app: AppReducer,
  login: loginReducer,
  changePassword: changePasswordReducer,
  forgotPassword: forgotPasswordReducer,
  register: registerReducer,
  verifyForgotPassword: VerifyForgotPasswordReducer,
  productDetails: productDetailsReducer,
  home: homeReducer,
  cart: cartReducer,
  addAddress: addAddressReducer,
  listAddress: listAddressReducer,
  changeProfile: changeProfileReducer,
  account: accountReducer
})

export default rootReducer
