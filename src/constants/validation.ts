export const CHANGE_EMAIL = "CHANGE_EMAIL"
export const CHANGE_PASSWORD = "CHANGE_PASSWORD"
export const CHANGE_ERROR = "CHANGE_ERROR"
export const CHANGE_PASSWORD_CONFIRM = "CHANGE_PASSWORD_CONFIRM"
export const PASSWORD_MIN_LENGTH = 6

export const WRONG_PASSWORD = "Oops! Your Password Is Not Correct"
export const WRONG_EMAIL = "Oops! Your Email Is Not Correct"
export const MIN_PASSWORD = `Password should be longer than ${PASSWORD_MIN_LENGTH} characters`
export const INVALID_EMAIL = "This field is not a valid email!"
export const REQUIRE = "This field is required!"
