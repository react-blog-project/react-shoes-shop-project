export const PATH = {
  HOME: "/",
  PRODUCT: "/product",
  LOGIN: "/login",
  VERIFY_FORGOT_PASSWORD: "/verify-forgot-password",
  ELEMENT: "/element",
  CART: "/cart",
  REGISTER: "/register",
  PROFILE: "/profile",
  CHANGE_PROFILE: "/change-profile",
  FORGOT_PASSWORD: "/forgot-password",
  CHANGE_PASSWORD: "/change-password",
  ACCOUNT: "/account",
  ORDER: "/order",
  ADDRESS: "/address",
  ADD_ADDRESS: "/add-address",
  PAYMENT: "/payment"
}

export const CURRENCY_DEFAULT = "$"
