import * as types from "./App.constants"

export const getProfileRequest = () => ({
  type: types.USER_REQUEST_INFO
})

export const getProfileSuccess = (user: UserInterface) => ({
  type: types.USER_REQUEST_INFO_SUCCESS,
  user
})

export const getProfileFailure = (errorMessage: string) => ({
  type: types.USER_REQUEST_INFO_FAILURE,
  errorMessage
})
