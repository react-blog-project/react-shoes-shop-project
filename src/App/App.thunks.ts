import { getProfile } from "../apis/user.api"
import * as actionApp from "./App.actions"

export const getUserInfo = () => dispatch => {
  dispatch(actionApp.getProfileRequest())

  return getProfile()
    .then(res => {
      dispatch(actionApp.getProfileSuccess(res.data))
    })
    .catch(err =>
      Promise.reject(dispatch(actionApp.getProfileFailure(err.message)))
    )
}
