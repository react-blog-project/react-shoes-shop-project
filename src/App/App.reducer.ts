import * as types from "./App.constants"
import * as typeChangeProfiles from "../pages/ChangeProfile/ChangeProfile.constants"
import * as typeAccount from "../pages/Account/Account.constants"
import produce from "immer"

interface AppInterface {
  loading: boolean
  errorMessage: string
  user: UserInterface
  isGetUser: boolean
}

const initialUser = {
  id: 0,
  email: "",
  mobile: null,
  firstName: "",
  lastName: "",
  gender: 3,
  birthday: null,
  status: "",
  createdAt: null,
  updatedAt: null,
  address: [],
  role: [],
  isTwoFactorAuthenticationEnabled: false
}

const initialState: AppInterface = {
  loading: false,
  errorMessage: "",
  user: initialUser,
  isGetUser: false
}

export const AppReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.USER_REQUEST_INFO:
        draft.loading = true
        break
      case types.USER_REQUEST_INFO_SUCCESS:
        draft.user = action.user as UserInterface
        draft.isGetUser = true
        draft.loading = false
        break
      case types.USER_REQUEST_INFO_FAILURE:
        draft.isGetUser = true
        draft.errorMessage = action.errorMessage
        draft.loading = false
        break
      case typeChangeProfiles.CHANGE_PROFILE_SUCCESS:
        draft.user.birthday = action.profile.birthday
        draft.user.firstName = action.profile.firstName
        draft.user.gender = action.profile.gender
        draft.user.lastName = action.profile.lastName
        draft.user.mobile = action.profile.mobile
        break
      case typeAccount.LOG_OUT_SUCCESS:
        draft.user = initialUser
        break
      default:
        return state
    }
  })
