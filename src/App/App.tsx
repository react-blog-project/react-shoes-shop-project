import { useEffect } from "react"
import { useDispatch } from "react-redux"
import { getUserInfo } from "./App.thunks"
import Routes from "../routes/routes"

function App() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getUserInfo())
  }, [dispatch])

  return <Routes />
}

export default App
