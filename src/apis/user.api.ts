import request from "../utils/request"

export const getProfile = () => {
  return request({
    url: "/users/profile",
    method: "get"
  })
}

export const postLogin = ({ email, password }: RequestLogin) => {
  return request({
    url: "/authentication/login",
    method: "post",
    data: { email, password }
  })
}

export const postForgotPassword = ({ email }: RequestResetPassword) => {
  return request({
    url: "/authentication/forgot-password",
    method: "post",
    data: { email }
  })
}

export const postRegister = ({
  firstName,
  lastName,
  email,
  password
}: RequestRegister) => {
  return request({
    url: "/authentication/register",
    method: "post",
    data: {
      firstName,
      lastName,
      email,
      password
    }
  })
}

export const postVerifyForgotPassword = ({
  password,
  token
}: RequestVerifyForgotPassword) => {
  return request({
    url: "/authentication/verify-forgot-password",
    method: "post",
    data: { password, token }
  })
}

export const patchChangePassword = ({
  oldPassword,
  newPassword
}: RequestChangePassword) => {
  return request({
    url: "users/password",
    method: "patch",
    data: { oldPassword, newPassword }
  })
}

export const postLogout = () => {
  return request({
    url: "/authentication/logout",
    method: "post"
  })
}
