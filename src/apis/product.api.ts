import request from "../utils/request"

export const getProductBySlugApi = (slug: string) => {
  return request({
    url: `/products/${slug}`
  })
}

export const getProductListApi = (page: number) => {
  return request({
    url: `/products?page=${page}&limit=8`,
    method: "get"
  })
}
