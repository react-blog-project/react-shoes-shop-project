import request from "../utils/request"

export const getProfile = () => {
  return request({
    url: "/users/profile",
    method: "get"
  })
}

export const patchChangeProfile = ({
  mobile,
  firstName,
  lastName,
  address,
  gender,
  birthday
}: UserInterface) => {
  return request({
    url: "users/profile",
    method: "patch",
    data: {
      mobile,
      firstName,
      lastName,
      address,
      gender,
      birthday
    }
  })
}
