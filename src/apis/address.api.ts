import axios from "axios"
import request from "../utils/request"

const URL_ADDRESS = "https://provinces.open-api.vn/api"

export const getProvinces = () => {
  return axios.get(`${URL_ADDRESS}/p`)
}

export const getDistricts = (province: number) => {
  return axios.get(`${URL_ADDRESS}/p/${province}?depth=2`)
}

export const getWards = (district: number) => {
  return axios.get(`${URL_ADDRESS}/d/${district}?depth=2`)
}

export const postAddress = (address: Address) => {
  return request({
    url: "/users/address",
    method: "post",
    data: {
      street: address.street,
      streetOptional: address.streetAddress2,
      ward: address.ward,
      district: address.district,
      province: address.province
    }
  })
}

export const deleteAddressApi = (id: number) => {
  return request({
    url: `/users/address/${id}`,
    method: "delete"
  })
}
