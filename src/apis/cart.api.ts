import request from "../utils/request"

export const getCart = () => {
  return request({
    url: "/carts",
    method: "get"
  })
}

export const updateCart = ({
  totalPrice,
  productId,
  quantity
}: IupdateCart) => {
  return request({
    url: "/carts",
    method: "post",
    data: { totalPrice, productId, quantity }
  })
}
