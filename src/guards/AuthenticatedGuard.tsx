import { ComponentType } from "react"
import {
  Route,
  RouteProps,
  Redirect,
  RouteComponentProps
} from "react-router-dom"
import { connect, useSelector } from "react-redux"
import * as appActions from "../App/App.thunks"
import PageLoader from "../components/Loading/PageLoader"

interface ReduxProps {
  getUserInfo: Function
}
interface Props extends ReduxProps, RouteProps {
  component: ComponentType<RouteComponentProps>
}

function AuthenticatedGuard(props: Props) {
  const userId = useSelector((state: AppState) => {
    if (state.app.user) return state.app.user.id
    return
  })

  const isGetUser = useSelector((state: AppState) => state.app.isGetUser)

  const { component: Component, ...rest } = props
  return (
    <Route
      {...rest}
      render={props => {
        if (!userId && isGetUser) {
          return <Redirect to="/login" />
        }
        if (userId) return <Component {...props} />
        if (!isGetUser) return <PageLoader />
      }}
    />
  )
}

const mapStateToProps = () => ({})

const mapDispatchToProps = {
  getUserInfo: appActions.getUserInfo
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticatedGuard)
