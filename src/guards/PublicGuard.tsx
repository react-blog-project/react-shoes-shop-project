import { ComponentType } from "react"
import {
  Route,
  RouteProps,
  RouteComponentProps,
  useHistory
} from "react-router-dom"
import { connect, useSelector } from "react-redux"
import * as appActions from "../App/App.thunks"
import { PATH } from "../constants/index"

interface ReduxProps {
  getUserInfo: Function
}
interface Props extends ReduxProps, RouteProps {
  component: ComponentType<RouteComponentProps>
}

function PublicGuard(props: Props) {
  const history = useHistory()
  const userId = useSelector((state: AppState) => {
    if (state.app.user) return state.app.user.id
    return
  })

  const { component: Component, ...rest } = props
  return (
    <Route
      {...rest}
      render={props => {
        if (!userId) {
          return <Component {...props} />
        } else history.push(PATH.HOME)
      }}
    />
  )
}

const mapStateToProps = () => ({})

const mapDispatchToProps = {
  getUserInfo: appActions.getUserInfo
}

export default connect(mapStateToProps, mapDispatchToProps)(PublicGuard)
