interface Address {
  id?: number
  street: string
  streetAddress2?: string | null
  ward: string
  district: string
  province: string
}
interface ResGetAddressApi extends Res {
  data: {
    address: Address[]
  }
}
interface ResGetAddress extends ActionRedux {}
