interface RequestLogin {
  email: string
  password: string
}
interface ResLoginApi extends Res {
  data: {
    access_token: string
  }
}
interface ResLogin extends ActionRedux {}

interface RequestResetPassword {
  email: string
}

interface ResResetPassword extends ActionRedux {}

interface RequestVerifyForgotPassword {
  password: string
  passwordConfirm: string
  token: string | null
}

interface ResVerifyForgotPassword extends ActionRedux {}

interface RequestRegister {
  firstName: string
  lastName: string
  email: string
  password: string
  passwordConfirm: string
}

interface ResRegister extends ActionRedux {}

interface RequestChangePassword {
  oldPassword: string
  newPassword: string
  passwordConfirm: string
}

interface ResChangePassword extends ActionRedux {}

interface UserInterface {
  id: number
  email: string
  mobile: string | null
  firstName: string
  lastName: string
  gender: number
  birthday: string | null
  status: string
  createdAt: Date | null
  updatedAt: Date | null
  address: any
  role: any
  isTwoFactorAuthenticationEnabled: boolean
}
