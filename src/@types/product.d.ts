interface Product {
  id: number
  name: string
  slug: string
  trademark: string
  rate: number
  sellingPrice: number
  salePrice: number
  gender: number
  description: string
  categories: [
    {
      id: number
      slug: string
    }
  ]
  images: [
    {
      name: string | null
      url: string
      width: null
      height: null
      description: null
    }
  ]
}

interface ProductDetails {
  id: number
  name: string
  slug: string
  trademark: string
  rate: number
  sellingPrice: number
  salePrice: number
  gender: number
  description: string
  createdAt: string
  updatedAt: string
  images: [
    {
      id: number
      name: string | null
      url: string
      width: string | null
      height: string | null
      description: string | null
    }
  ]
}

interface ResGetProductApi extends Res {
  products: Product[]
  totalCount: number
}

interface ResGetProduct extends ActionRedux {
  payload: ResGetProductApi
}

interface ResGetProductItemApi extends Res {
  product: ProductDetails
}

interface ResGetProductItem extends ActionRedux {
  payload: ResGetProductItemApi
}
