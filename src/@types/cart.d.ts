interface ProductCart {
  id: number
  totalPrice: number
  productId: number
  productNumber: number
  product: {
    id: number
    name: string
    trademark: string
    rate: number
    sellingPrice: number
    salePrice: number
    gender: number
    description: string
  }
}

interface IupdateCart {
  totalPrice: number
  productId: number
  quantity: number
}
