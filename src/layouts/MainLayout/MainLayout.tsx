import { ReactNode } from "react"
import Navigation from "../../components/Navigation/Navigation"
import TopMenu from "../../components/TopMenu/TopMenu"
import Footer from "../../components/Footer/Web/Footer"
import FooterMobile from "../../components/Footer/Mobile/FooterMobile"
import { BREAKPOINT } from "../../constants/styles"
import { useWindowSize } from "../../hooks/useWindowSize"
import "./MainLayout.scss"

interface Props {
  children: ReactNode
}

export default function MainLayout({ children }: Props) {
  const [width] = useWindowSize()
  const renderTopMenu = width <= BREAKPOINT.MD ? null : <TopMenu />
  const renderFooter = width <= BREAKPOINT.MD ? <FooterMobile /> : <Footer />

  return (
    <div className="wrapper d-flex align-items-stretch">
      <main className="flex-grow-1 mw-100 overflow-auto min-vh-100">
        {renderTopMenu}
        <Navigation />
        <div className="content mt-3">{children}</div>
        {renderFooter}
      </main>
    </div>
  )
}
