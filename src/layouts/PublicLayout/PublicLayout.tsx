import React, { ReactNode } from "react"
import { ReactComponent as LogoIcon } from "../../assets/images/logo.svg"
import PageLoader from "src/components/Loading/PageLoader"
import "./PublicLayout.scss"
interface Props {
  children: ReactNode
  title: string
  subTitle: string
  classStyle: string
  loading: boolean
}

export default function PublicLayout(props: Props) {
  const { children, title, subTitle, classStyle, loading } = props
  return (
    <div>
      <div className="container-fluid">
        <div className="row d-flex justify-content-center">
          <div className="layout d-flex shadow">
            <div className={`header text-center ${classStyle}`}>
              <LogoIcon />
              <p className="welcome">{title} </p>
              <p className="title">{subTitle}</p>
            </div>
            <div>{children}</div>
          </div>
        </div>
      </div>
      {loading && <PageLoader />}
    </div>
  )
}
