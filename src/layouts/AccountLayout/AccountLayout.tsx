import React, { ReactNode } from "react"
import { Link } from "react-router-dom"
import { ReactComponent as IconLeft } from "../../assets/images/icon_left.svg"
import SEO from "../../components/SEO/SEO"
import TitleHeader from "../../components/TitleHeader/TitleHeader"
import "./AccountLayout.scss"
interface Props {
  children: ReactNode
  title: string
  pathBack: string
}

export default function AccountLayout(props: Props) {
  const { children, title, pathBack } = props
  return (
    <div className="d-flex justify-content-center">
      <SEO title={title} description={`This is ${title} page`} />
      <div className="account-layout shadow">
        <TitleHeader content={title}>
          <Link to={`/${pathBack}`}>
            <IconLeft />
          </Link>
        </TitleHeader>
        {children}
      </div>
    </div>
  )
}
