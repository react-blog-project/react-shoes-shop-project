Shoes shop ([Figma Link](<https://www.figma.com/file/yI87tOx56uKP6mSzfnmE3D/E-comm-App-Kit-(Community)?node-id=0%3A1>)) created by [create-react-app](https://reactjs.org/docs/create-a-new-react-app.html).

# Technical using

- [React](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Redux](https://redux.js.org/)
- [Redux-thunk](https://github.com/reduxjs/redux-thunk)
- [Sass](https://sass-lang.com/)
- [Bootstrap](https://getbootstrap.com/)
- [Immer](https://github.com/immerjs/immer)

# Folder structure

```bash
├───.vscode
├───public
└───src
    ├───@types
    ├───apis
    ├───App
    ├───assets
    │   └───images
    ├───components
    │   ├───Header
    │   └───SideNav
    ├───constants
    ├───guards
    ├───helpers
    ├───hooks
    ├───layouts
    ├───pages
    │   ├───Home
    │   ├───Login
    │   └───Product
    │       ├───ProductItem
    │       └───ProductList
    ├───reducer
    ├───routes
    └───store
```

# Getting started

## Create .env file in project and copy this code

```bash
REACT_APP_API_URL = http://localhost:4001/api
```

## Installing package modules

```bash
npm i
# or
yarn
```

## Run development server

```bash
npm start
# or
yarn start
```

Open http://localhost:3000 to view it in the browser.

## Login

```bash
- username: admin
- password: 123
```

## Link to server backend

```bash

```
