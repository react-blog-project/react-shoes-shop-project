# Changelog

Shoes Shop, [Figma link](<https://www.figma.com/file/yI87tOx56uKP6mSzfnmE3D/E-comm-App-Kit-(Community)?node-id=0%3A1>)

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed

- Convert from [styled-component](https://styled-components.com/) to [scss](https://sass-lang.com/)
- Loading Component by design

### Added

- Write mini component Form.
- Create Element page.
- Logic for login page instead of mock API.
- Create register page
- Create Main Layout (Top Menu, Navigation, Navigation Mobile, Footer, TabBar in Mobile)
- Create Public Layout

## [1.0.0] - 2021-04-24

### Added

- React boilerplate using [typescript](https://www.typescriptlang.org/docs/)
- Apply [Redux](https://react-redux.js.org/introduction/getting-started) to project
- Apply [Redux-thunk](https://github.com/reduxjs/redux-thunk) to project
- Style using [styled-component](https://styled-components.com/)
